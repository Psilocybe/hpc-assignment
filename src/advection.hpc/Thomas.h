///
/// \file Thomas.h
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Thomas algorithm - three diagnoal matrix solver.
///
/// Quick 3-diagonal matrix solver based on Thomas algorithm.
//////////////////////////////////////////////////////////////////////////

#ifndef _THOMAS_H
#define _THOMAS_H

#include "MPITools.h"

#include <vector>

namespace advection
{
	namespace tool
	{
		/// \brief MPI parallel LU matrix decomposition for Thomas algorithm.
		/// Simplified version for solving the 3-diagonal system [b a c]*X=Q,
		/// with matrices with a single constant value on each of 3 diagonals.
		/// Upper diagonal values are the same as upper value of original matrix.
		///
		/// @param mpiInfo Informations about current MPI configuration.
		/// @param lower Value on lower diagonal of original matrix.
		/// @param middle Value on middle diagonal of original matrix.
		/// @param upper Value on upper diagonal of original matrix.
		/// @param l Lower diagonal result of LU decomposition.
		/// @param d Middle diagonal result of LU decomposition.
		void ThomasAlgorithmParallelLU(const MPIInfo& mpiInfo,
			double lower, double middle, double upper,
			std::vector<double>& l, std::vector<double>& d);

		/// \brief MPI parallel LU matrix solver for Thomas algorithm.
		/// Simplified version for solving the 3-diagonal system [b a c]*X=Q,
		/// with matrices with a single constant value on each of 3 diagonals.
		///
		/// @param mpiInfo Informations about current MPI configuration.
		/// @param l Value of lower diagonal of LU matrix.
		/// @param d Value of middle diagonal of LU matrix.
		/// @param u Value of upper diagonal of LU matrix.
		/// @param q Vector Q of constant terms.
		/// @return Solution vector X.
		std::vector<double> ThomasAlgorithmParallelSolve(const MPIInfo& mpiInfo,
			const std::vector<double>& l, const std::vector<double>& d,
			const double& u, const std::vector<double>& q);
	}
}

#endif
