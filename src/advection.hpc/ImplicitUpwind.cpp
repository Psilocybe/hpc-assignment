///
/// \file ImplicitUpwind.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Implementation of implicit upwind solver.
//////////////////////////////////////////////////////////////////////////

#include "ImplicitUpwind.h"

namespace advection
{
	namespace scheme
	{
		bool ImplicitUpwind::isStable()
		{
			return true;
		}

		std::vector<double> ImplicitUpwind::calculateStep(std::vector<double> previousStep)
		{
			std::vector<double> currentStep(localGridSize);
			double leftBoundaryValue = getLeftBoundaryValue();
			if (tool::isMaster(mpiInfo))
			{
				currentStep.front() = leftBoundaryValue;
			}
			else
			{
				currentStep[0] = (previousStep[0] + cfl * leftBoundaryValue) / (1.0 + cfl);
			}
			for (int g = 1; g < localGridSize; g++)
			{
				currentStep[g] = (previousStep[g] + cfl * currentStep[g - 1]) / (1.0 + cfl);
			}
			if (tool::isLastNode(mpiInfo))
			{
				currentStep.back() = previousStep.back();
			}
			sendRightBoundaryValue(currentStep.back());
			return currentStep;
		}

		ImplicitUpwind::ImplicitUpwind(IO::AdvectionConfiguration& configuration, tool::MPIInfo& mpiInfo)
			: IScheme(configuration, mpiInfo)
		{
		}
	}
}
