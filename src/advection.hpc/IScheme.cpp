///
/// \file IScheme.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Implementation of non-abstract methods.
//////////////////////////////////////////////////////////////////////////

#include "IScheme.h"

#include <cmath>
#include <functional>
#include <stdexcept>

namespace advection
{
	namespace scheme
	{
		// SOLVER
		//////////////////////////////////////////////////////////////////////////

		void IScheme::solve()
		{
			if (!isStable())
				throw std::domain_error("Scheme is unstable for given CFL.");
			currentTime = 0.0;
			prepareContainers();
			std::vector<double> step = initialize();
			// Time levels loop
			for (int tl = 0; tl < timeLevels.size(); tl++)
			{
				// Main time loop
				while (currentTime <= timeLevels[tl])
				{
					currentTime += deltaTime;
					step = calculateStep(step);
				}
				actualTimeLevels[tl] = currentTime;
				numericalValues[tl] = step;
			}
			finalizeParallelComputations();
		}

		// MPI METHODS
		//////////////////////////////////////////////////////////////////////////

		double IScheme::getLeftBoundaryValue()
		{
			double leftBoundaryValue = 0.0;
			if (!tool::isMaster(mpiInfo))
			{
				MPI_Status status;
				MPI_Recv(&leftBoundaryValue, 1, MPI_DOUBLE, mpiInfo.nodeId - 1, tool::BOUNDARY_VALUE, MPI_COMM_WORLD, &status);
			}
			return leftBoundaryValue;
		}

		void IScheme::sendRightBoundaryValue(double rightBoundaryValue)
		{
			if (!tool::isLastNode(mpiInfo))
			{
				MPI_Send(&rightBoundaryValue, 1, MPI_DOUBLE, mpiInfo.nodeId + 1, tool::BOUNDARY_VALUE, MPI_COMM_WORLD);
			}
		}

		void IScheme::finalizeParallelComputations()
		{
			if (tool::isMaster(mpiInfo))
			{
				// Numerical values vector at master node will contain complete solutions,
				// so to collect and store calculated vectors from other nodes, we need to resize this container.
				for (int t = 0; t < timeLevels.size(); t++)
				{
					numericalValues[t].resize(gridSize);
				}
				grid.resize(gridSize);
				int index = localGridSize;
				for (int i = 1; i < mpiInfo.totalNodes; i++)
				{
					MPI_Status status;
					tool::MPIInfo nodeInfo; 
					nodeInfo.nodeId = i;
					nodeInfo.totalNodes = mpiInfo.totalNodes;
					int amountToReceiveFromNodeI = tool::getLocalVectorSize(nodeInfo, gridSize);
					for (int t = 0; t < timeLevels.size(); t++)
					{
						MPI_Recv(&numericalValues[t][index], amountToReceiveFromNodeI, MPI_DOUBLE, i, tool::NUMERICAL_VALUES, MPI_COMM_WORLD, &status);
					}
					MPI_Recv(&grid[index], amountToReceiveFromNodeI, MPI_DOUBLE, i, tool::GRID_VALUES, MPI_COMM_WORLD, &status);
					index += amountToReceiveFromNodeI;
				}
				calculateError();
			}
			else
			{
				for (int t = 0; t < timeLevels.size(); t++)
				{
					MPI_Send(&numericalValues[t][0], localGridSize, MPI_DOUBLE, 0, tool::NUMERICAL_VALUES, MPI_COMM_WORLD);
				}
				MPI_Send(&grid[0], localGridSize, MPI_DOUBLE, 0, tool::GRID_VALUES, MPI_COMM_WORLD);
			}
		}

		// INTERNAL
		//////////////////////////////////////////////////////////////////////////

		void IScheme::calculateError()
		{
			for (int timeStep = 0; timeStep < timeLevels.size(); timeStep++)
			{
				std::vector<double> error(gridSize);
				for (int i = 0; i < gridSize; i++)
				{
					double analytical = solveAnalytically(grid[i], actualTimeLevels[timeStep], acceleration);
					analyticalValues[timeStep].push_back(analytical);
					error[i] = analytical - numericalValues[timeStep][i];
				}
				errorNorms[timeStep] = tool::calculateNorms(error);
			}
		}

		void IScheme::prepareContainers()
		{
			// Resize vectors for speedup
			grid = std::vector<double>(localGridSize);
			actualTimeLevels = std::vector<double>(timeLevels.size());
			errorNorms = std::vector<tool::Norms>(timeLevels.size());
			numericalValues = std::vector<std::vector<double> >(timeLevels.size());
			analyticalValues = std::vector<std::vector<double> >(timeLevels.size());
			// Intialize grid points
			double x = lowerBoundary + tool::getIndexInGlobalVector(mpiInfo, gridSize, 0) * deltaSpace;
			for (int i = 0; i < localGridSize; i++, x += deltaSpace)
			{
				grid[i] = x;
			}
		}

		double IScheme::calculateDeltaTime() const
		{
			return (cfl * deltaSpace) / acceleration;
		}

		std::vector<double> IScheme::initialize()
		{
			std::vector<double> initialStep(localGridSize);
			for (int i = 0; i < localGridSize; i++)
			{
				initialStep[i] = solveAnalytically(grid[i], currentTime, acceleration);
			}
			return initialStep;
		}

		// CONSTRUCTORS
		//////////////////////////////////////////////////////////////////////////

		IScheme::IScheme(IO::AdvectionConfiguration& configuration, tool::MPIInfo info)
		{
			mpiInfo = info;
			currentTime = 0.0;
			cfl = configuration.cfl;
			gridSize = configuration.gridSize;
			localGridSize = tool::getLocalVectorSize(mpiInfo, gridSize);
			lowerBoundary = configuration.lowerBoundary;
			upperBoundary = configuration.upperBoundary;
			acceleration = configuration.acceleration;
			solveAnalytically = configuration.initFunction;
			timeLevels = configuration.timeLevels;
			deltaSpace = (configuration.upperBoundary - configuration.lowerBoundary) / (gridSize - 1);
			deltaTime = calculateDeltaTime();
		}

		IScheme::~IScheme()
		{
		}

		// ACCESSOR METHODS
		//////////////////////////////////////////////////////////////////////////

		double IScheme::getDeltaSpace() const
		{
			return deltaSpace;
		}

		double IScheme::getDeltaTime() const
		{
			return deltaTime;
		}

		double IScheme::getCFL() const
		{
			return cfl;
		}

		std::vector<double> IScheme::getGrid() const
		{
			return grid;
		}

		std::vector<std::vector<double> > IScheme::getAnalyticalValues() const
		{
			return analyticalValues;
		}

		std::vector<tool::Norms> IScheme::getNorms() const
		{
			return errorNorms;
		}

		std::vector<std::vector<double> > IScheme::getNumericalValues() const
		{
			return numericalValues;
		}
	}
}
