///
/// \file ExplicitUpwind.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Implementation of explicit upwind solver.
//////////////////////////////////////////////////////////////////////////

#include "ExplicitUpwind.h"

namespace advection
{
	namespace scheme
	{
		bool ExplicitUpwind::isStable()
		{
			return cfl <= 1.0;
		}

		std::vector<double> ExplicitUpwind::calculateStep(std::vector<double> previousStep)
		{
			std::vector<double> currentStep(localGridSize);
			double leftBoundaryValue = getLeftBoundaryValue();
			sendRightBoundaryValue(previousStep.back());
			if (tool::isMaster(mpiInfo))
			{
				currentStep.front() = leftBoundaryValue;
			}
			else
			{
				currentStep[0] = previousStep[0] + cfl * (leftBoundaryValue - previousStep[0]);
			}
			for (int g = 1; g < localGridSize; g++)
			{
				currentStep[g] = previousStep[g] + cfl * (previousStep[g - 1] - previousStep[g]);
			}
			if (tool::isLastNode(mpiInfo))
			{
				currentStep.back() = previousStep.back();
			}
			return currentStep;
		}

		ExplicitUpwind::ExplicitUpwind(IO::AdvectionConfiguration& configuration, tool::MPIInfo& mpiInfo)
			: IScheme(configuration, mpiInfo)
		{
		}
	}
}
