///
/// \file ImplicitUpwind.h
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Declaration of advection solver based on implicit upwind scheme.
//////////////////////////////////////////////////////////////////////////

#ifndef _IMPLICIT_UPWIND_H
#define _IMPLICIT_UPWIND_H

#include "IScheme.h"

namespace advection
{
	namespace scheme
	{
		/// @brief Advection equation solver implementing implicit upwind scheme.
		///
		/// This solver forward-time backward-space scheme which is actually
		/// 2-diagonal matrix, which can be solved using forward substitution.
		class ImplicitUpwind :
			public IScheme
		{
		protected:
			/// Calculates values for next time step.
			///
			/// @param previousStep Vector of values from previous step.
			/// @return Calculated vector.
			std::vector<double> calculateStep(std::vector<double> previousStep);

		public:
			/// Initializes scheme by configuration parameters.
			///
			/// @param configuration Configuration parameters for simulation.
			/// @param mpiInfo Informations about current MPI configuration.
			explicit ImplicitUpwind(IO::AdvectionConfiguration& configuration, tool::MPIInfo& mpiInfo);

			/// Checks if scheme is stable for internal cfl number.
			///
			/// @return True for stable scheme, false otherwise.
			bool isStable();
		};
	}
}

#endif
