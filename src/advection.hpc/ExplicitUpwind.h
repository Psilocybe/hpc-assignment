///
/// \file ExplicitUpwind.h
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Declaration of advection solver based on explicit upwind scheme.
//////////////////////////////////////////////////////////////////////////

#ifndef _EXPLICIT_UPWIND_H
#define _EXPLICIT_UPWIND_H

#include "IScheme.h"

namespace advection
{
	namespace scheme
	{
		/// Advection equation solver implementing explicit upwind scheme.
		class ExplicitUpwind
			: public IScheme
		{
		protected:
			/// Calculates values for next time step.
			///
			/// @param previousStep Vector of values from previous step.
			/// @return Calculated vector.
			std::vector<double> calculateStep(std::vector<double> previousStep);

		public:
			/// Initializes scheme by configuration parameters.
			///
			/// @param configuration Configuration parameters for simulation.
			/// @param mpiInfo Informations about current MPI configuration.
			explicit ExplicitUpwind(IO::AdvectionConfiguration &configuration, tool::MPIInfo& mpiInfo);
			
			/// Checks if scheme is stable for internal cfl number.
			///
			/// @return True for stable scheme, false otherwise.
			bool isStable();
		};
	}
}

#endif
