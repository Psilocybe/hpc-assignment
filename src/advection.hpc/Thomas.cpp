///
/// \file Thomas.cpp
///
/// \author Bartlomiej Szostek
/// \date Jaunuary 2017
///
/// \brief Implementation of Thomas algorithm.
///
/// Implementations of Thomas 3-diagonal matrix solver.
//////////////////////////////////////////////////////////////////////////

#include <mpi.h>

#include "Thomas.h"

#include <cmath>

namespace advection
{
	namespace tool
	{
		void ThomasAlgorithmParallelLU(const MPIInfo& mpiInfo,
			double lower, double middle, double upper,
			std::vector<double>& l, std::vector<double>& d)
		{
			MPI_Status status;
			int size = l.size();
			double S[2][2] = {{1.0, 0.0}, {0.0, 1.0}}, T[2][2];
			std::vector<double> y(size, 0.0), result(size);
			int rowsLocal = (int)floor((double)size / mpiInfo.totalNodes);
			int offsetLocal = mpiInfo.nodeId * rowsLocal;
			// Form local products of R_k matrices
			if (tool::isMaster(mpiInfo))
			{
				double s1tmp = middle * S[0][0];
				S[1][0] = S[0][0];
				S[1][1] = S[0][1];
				S[0][1] = middle * S[0][1];
				S[0][0] = s1tmp;
				for (int i = 1; i < rowsLocal; i++)
				{
					s1tmp = middle * S[0][0] - lower * upper * S[1][0];
					double s2tmp = middle * S[0][1] - lower * upper * S[1][1];
					S[1][0] = S[0][0];
					S[1][1] = S[0][1];
					S[0][0] = s1tmp;
					S[0][1] = s2tmp;
				}
			}
			else
			{
				for (int i = 0; i < rowsLocal; i++)
				{
					double s1tmp = middle * S[0][0] - lower * upper * S[1][0];
					double s2tmp = middle * S[0][1] - lower * upper * S[1][1];
					S[1][0] = S[0][0];
					S[1][1] = S[0][1];
					S[0][0] = s1tmp;
					S[0][1] = s2tmp;
				}
			}
			// Full-recursive-doubling algorithm for distribution
			for (int i = 0; i <= log2(mpiInfo.totalNodes); i++)
			{
				if ((mpiInfo.nodeId + pow(2.0, i)) < mpiInfo.totalNodes)
				{
					MPI_Send(S, 4, MPI_DOUBLE, int(mpiInfo.nodeId + pow(2.0, i)), tool::THOMAS_DISTRIBUTE, MPI_COMM_WORLD);
				}
				if ((mpiInfo.nodeId - pow(2.0, i)) >= 0)
				{
					MPI_Recv(T, 4, MPI_DOUBLE, int(mpiInfo.nodeId - pow(2.0, i)), tool::THOMAS_DISTRIBUTE, MPI_COMM_WORLD, &status);
					double s1tmp = S[0][0] * T[0][0] + S[0][1] * T[1][0];
					S[0][1] = S[0][0] * T[0][1] + S[0][1] * T[1][1];
					S[0][0] = s1tmp;
					s1tmp = S[1][0] * T[0][0] + S[1][1] * T[1][0];
					S[1][1] = S[1][0] * T[0][1] + S[1][1] * T[1][1];
					S[1][0] = s1tmp;
				}
			}
			// Calculate last d_k first, so that it can be distributed, and distribute it
			d[offsetLocal + rowsLocal - 1] = (S[0][0] + S[0][1]) / (S[1][0] + S[1][1]);
			if (tool::isMaster(mpiInfo))
			{
				MPI_Send(&d[offsetLocal + rowsLocal - 1], 1, MPI_DOUBLE, 1, tool::THOMAS_DISTRIBUTE, MPI_COMM_WORLD);
			}
			else
			{
				MPI_Recv(&d[offsetLocal - 1], 1, MPI_DOUBLE, mpiInfo.nodeId - 1, tool::THOMAS_DISTRIBUTE, MPI_COMM_WORLD, &status);
				if (!tool::isLastNode(mpiInfo))
				{
					MPI_Send(&d[offsetLocal + rowsLocal - 1], 1, MPI_DOUBLE, mpiInfo.nodeId + 1, tool::THOMAS_DISTRIBUTE, MPI_COMM_WORLD);
				}
			}

			// Compute in parallel the local values of d_k and l_k
			if (tool::isMaster(mpiInfo))
			{
				d[0] = middle;
				for (int i = 0; i < rowsLocal - 1; i++)
				{
					l[i] = lower / d[i];
					d[i + 1] = middle - l[i] * upper;
				}
				l[rowsLocal - 1] = lower / d[rowsLocal - 1];
				d[rowsLocal - 1] = middle - l[rowsLocal - 2] * upper;
			}
			else
			{
				for (int i = 0; i < rowsLocal - 1; i++)
				{
					l[offsetLocal + i] = lower / d[offsetLocal + i - 1];
					d[offsetLocal + i] = middle - l[offsetLocal + i] * upper;
				}
				l[offsetLocal + rowsLocal - 1] = lower / d[offsetLocal + rowsLocal - 2];
			}
			if (!tool::isMaster(mpiInfo))
			{
				d[offsetLocal - 1] = 0;
			}
			// Distribute d_k and l_k to all nodes
			std::vector<double> tmp = d;
			MPI_Allreduce(&tmp[0], &d[0], size, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
			tmp = l;
			MPI_Allreduce(&tmp[0], &l[0], size, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
		}

		std::vector<double> ThomasAlgorithmParallelSolve(const MPIInfo& mpiInfo,
			const std::vector<double>& l, const std::vector<double>& d,
			const double& u, const std::vector<double>& q)
		{
			MPI_Status status;
			int size = q.size();
			std::vector<double> result(size), y(size);
			int offsetLocal = tool::getIndexInGlobalVector(mpiInfo, l.size(), 0);
			// Forward Substitution [L][y] = [q]
			if (tool::isMaster(mpiInfo))
			{
				y[0] = q[0];
			}
			else
			{
				double leftY;
				MPI_Recv(&leftY, 1, MPI_DOUBLE, mpiInfo.nodeId - 1, tool::THOMAS_DISTRIBUTE_Y, MPI_COMM_WORLD, &status);
				y[0] = q[0] - l[offsetLocal - 1] * leftY;
			}
			for (int i = 1; i < size; i++)
			{
				y[i] = q[i] - l[offsetLocal + i - 1] * y[i - 1];
			}
			if (!tool::isLastNode(mpiInfo))
			{
				MPI_Send(&y[size - 1], 1, MPI_DOUBLE, mpiInfo.nodeId + 1, tool::THOMAS_DISTRIBUTE_Y, MPI_COMM_WORLD);
			}

			// Backward Substitution [U][x] = [y]
			if (tool::isLastNode(mpiInfo))
			{
				result[size - 1] = y[size - 1] / d[offsetLocal + size - 1];

			}
			else
			{
				double rightX;
				MPI_Recv(&rightX, 1, MPI_DOUBLE, mpiInfo.nodeId + 1, tool::THOMAS_DISTRIBUTE_X, MPI_COMM_WORLD, &status);
				result[size - 1] = (y[size - 1] - u * rightX) / d[offsetLocal + size - 1];
			}
			for (int i = size - 2; i >= 0; i--)
			{
				result[i] = (y[i] - u * result[i + 1]) / d[offsetLocal + i];
			}
			if (!tool::isMaster(mpiInfo))
			{
				MPI_Send(&result[0], 1, MPI_DOUBLE, mpiInfo.nodeId - 1, tool::THOMAS_DISTRIBUTE_X, MPI_COMM_WORLD);
			}
			return result;
		}
	}
}
