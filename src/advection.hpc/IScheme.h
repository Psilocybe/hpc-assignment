///
/// \file IScheme.h
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Declaration of abstract base class for all schemes.
//////////////////////////////////////////////////////////////////////////

#ifndef _I_SCHEME_H
#define _I_SCHEME_H

#include <mpi.h>

#include "AdvectionConfiguration.h"
#include "Norms.h"
#include "MPITools.h"

#include <string>
#include <vector>

namespace advection
{
	/// Encapsulates advection equation solvers.
	namespace scheme
	{
		/// @brief Base class for all schemes and factory of schemes.
		///
		/// IScheme is responsible for creation of schemes based on its name.
		/// This class is also base class for all schemes, it handles
		/// initialization, error calculation by means of analytical solution.
		class IScheme
		{
			/// Container for calculated norms of error vector for each time level.
			std::vector<tool::Norms> errorNorms;

			/// Calculates error norms for all time levels.
			/// Norms are saved in norm container.
			void calculateError();

			/// Resizes and zeros all containers for better performance.
			void prepareContainers();

		protected:
			/// Informations about current MPI configuration.
			tool::MPIInfo mpiInfo;

			/// Discretized grid size.
			int gridSize;

			/// Size of the grid for currently working node.
			int localGridSize;

			/// Container for calculated numerical values.
			std::vector<std::vector<double> > numericalValues;

			/// Container for calculated analytical values.
			std::vector<std::vector<double> > analyticalValues;

			/// Courant-Friedrichs-Lewy number.
			double cfl;

			/// Step in time.
			double deltaTime;

			/// Lower domain point.
			double lowerBoundary;

			/// Upper domain point.
			double upperBoundary;

			/// Step in space.
			double deltaSpace;

			/// Current time of simulation.
			double currentTime;

			/// Acceleration parameter of advection equation.
			double acceleration;

			/// Pointer to initialization function.
			double(*solveAnalytically) (double x, double time, double acceleration);

			/// Exact points in discretized domain.
			std::vector<double> grid;

			/// Vector of time levels, at which simulation saves values.
			std::vector<double> timeLevels;

			/// Vector of actual exact time levels, reached by simulation.
			std::vector<double> actualTimeLevels;

			/// Calculates delta time step for internal Courant-Friedrichs-Lewy number.
			///
			/// @return Delta time.
			/// @throws invalid_argument When given CFL makes scheme unstable.
			double calculateDeltaTime() const;

			/// Initializes values of initial wave with analytical solution.
			///
			/// @return Values calculated by initial function and boundary conditions.
			virtual std::vector<double> initialize();

			/// Calculates values for next time step.
			///
			/// @param previousStep Vector of values from previous step.
			/// @return Calculated vector.
			virtual std::vector<double> calculateStep(std::vector<double> previousStep) = 0;

			// MPI METHODS
			//////////////////////////////////////////////////////////////////////////

			/// Requests for left boundary value for the current node.
			/// In case of first node (master) it is analytical value.
			///
			/// @return Left boundary value for current node.
			double getLeftBoundaryValue();

			/// For each node, except the last one, sends right boundary value
			/// to the next node.
			///
			/// @param rightBoundaryValue Right boundary value.
			void sendRightBoundaryValue(double rightBoundaryValue);

			/// Collects data from each of working nodes to master node.
			void finalizeParallelComputations();

		public:
			// SOLVER
			//////////////////////////////////////////////////////////////////////////

			/// Launches scheme solver.
			/// Until the current time reaches highest time level, algorithm will
			/// continue to calculate next time step values and increase time by
			/// provided delta time.
			void solve();

			/// Checks if scheme is stable for internal cfl number.
			///
			/// @return True for stable scheme, false otherwise.
			virtual bool isStable() = 0;

			// CONSTRUCTORS
			//////////////////////////////////////////////////////////////////////////

			/// Initializes scheme by configuration parameters.
			///
			/// @param configuration Configuration parameters for simulation.
			/// @param info Informations about current MPI configuration.
			explicit IScheme(IO::AdvectionConfiguration& configuration, tool::MPIInfo info);

			/// Virtual destructor for proper cleaning up.
			virtual ~IScheme();

			// ACCESSOR METHODS
			//////////////////////////////////////////////////////////////////////////

			/// Get step in space for grid in this simulation.
			///
			/// @return Step in space.
			double getDeltaSpace() const;

			/// Get step in time in this simulation.
			///
			/// @return Step in time.
			double getDeltaTime() const;

			/// Get Courant-Friedrichs-Lewy number for current scheme.
			///
			/// @return CFL number.
			double getCFL() const;

			/// Get discretized domain values.
			///
			/// @return Discretized grid.
			std::vector<double> getGrid() const;

			/// Get calculated numerical values for each time level.
			///
			/// @return Numerical values.
			std::vector<std::vector<double> > getNumericalValues() const;

			/// Get calculated analytical values for each time level.
			///
			/// @return Analytical values.
			std::vector<std::vector<double> > getAnalyticalValues() const;

			/// Get error norms of error vector for each time level.
			///
			/// @return Error norms.
			std::vector<tool::Norms> getNorms() const;
		};
	}
}

#endif
