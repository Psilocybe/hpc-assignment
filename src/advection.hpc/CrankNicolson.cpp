///
/// \file CrankNicolson.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Implementation of Crank-Nicolson solver.
//////////////////////////////////////////////////////////////////////////

#include "CrankNicolson.h"
#include "Thomas.h"

namespace advection
{
	namespace scheme
	{
		bool CrankNicolson::isStable()
		{
			return true;
		}

		std::vector<double> CrankNicolson::calculateStep(std::vector<double> previousStep)
		{
			MPI_Status status;
			std::vector<double> currentStep(localGridSize);
			// Exchange boundary values
			double leftBoundaryValue, rightBoundaryValue;
			int senderId = (mpiInfo.nodeId - 1 + mpiInfo.totalNodes) % mpiInfo.totalNodes;
			int receiverId = (mpiInfo.nodeId + 1) % mpiInfo.totalNodes;
			MPI_Sendrecv(
				&previousStep.front(), 1, MPI_DOUBLE, senderId, tool::CRANK_RIGHT,
				&rightBoundaryValue, 1, MPI_DOUBLE, receiverId, tool::CRANK_RIGHT,
				MPI_COMM_WORLD, &status);
			MPI_Sendrecv(
				&previousStep.back(), 1, MPI_DOUBLE, receiverId, tool::CRANK_LEFT,
				&leftBoundaryValue, 1, MPI_DOUBLE, senderId, tool::CRANK_LEFT,
				MPI_COMM_WORLD, &status);
			// Main grid loop
			currentStep[0] = previousStep[0] - c * (previousStep[1] - leftBoundaryValue);
			for (int g = 1; g < localGridSize - 1; g++)
			{
				currentStep[g] = previousStep[g] - c * (previousStep[g + 1] - previousStep[g - 1]);
			}
			currentStep.back() = previousStep.back() - c * (rightBoundaryValue - previousStep[localGridSize - 2]);
			// Solve parallely 3-diagonal condensed matrix
			return tool::ThomasAlgorithmParallelSolve(mpiInfo, lower, middle, upper, currentStep);
		}

		CrankNicolson::CrankNicolson(IO::AdvectionConfiguration& configuration, tool::MPIInfo& mpiInfo)
			: IScheme(configuration, mpiInfo)
		{
			c = 0.25 * cfl;
			lower = std::vector<double>(gridSize, 0.0);
			middle = std::vector<double>(gridSize, 0.0);
			upper = c;
			tool::ThomasAlgorithmParallelLU(mpiInfo, -c, 1.0, c, lower, middle);
		}
	}
}
