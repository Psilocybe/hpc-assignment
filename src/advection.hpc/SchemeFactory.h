///
/// \file SchemeFactory.h
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Declaration of scheme factory.
//////////////////////////////////////////////////////////////////////////

#ifndef _SCHEME_FACTORY_H
#define _SCHEME_FACTORY_H

#include "IScheme.h"
#include "AdvectionConfiguration.h"
#include "MPITools.h"

#include <string>

namespace advection
{
	namespace scheme
	{
		// Factory for advection solvers.
		class SchemeFactory
		{
		public:
			/// Scheme factory method.
			/// Returns initialized scheme by its name.
			///
			/// @param schemeName Name of the scheme.
			/// @param configuration Configuration structure to initialize scheme.
			/// @param mpiInfo Informations about current MPI configuration.
			/// @return Initialized scheme for simulation.
			/// @throws invalid_argument When scheme name is not recognized.
			static IScheme* makeScheme(std::string schemeName, IO::AdvectionConfiguration& configuration, tool::MPIInfo& mpiInfo);
		};
	}
}

#endif
