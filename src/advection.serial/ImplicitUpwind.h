///
/// \file ImplicitUpwind.h
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Declaration of advection solver based on implicit upwind scheme.
//////////////////////////////////////////////////////////////////////////

#ifndef _IMPLICIT_UPWIND_H
#define _IMPLICIT_UPWIND_H

#include "IScheme.h"

namespace advection
{
	namespace scheme
	{
		/// @brief Advection equation solver implementing implicit upwind scheme.
		///
		/// This solver forward-time backward-space scheme which is actually 
		/// 2-diagonal matrix, which can be solved using forward substitution.
		class ImplicitUpwind :
			public IScheme
		{
			/// Solves PDE for positive CFL numbers.
			///
			/// @param currentStep Vector for currently calculated grid points.
			/// @param previousStep Vector of values calculated in previous time step.
			void forward(std::vector<double>& currentStep, const std::vector<double>& previousStep) const;

		protected:
			/// Calculates values for next time step.
			///
			/// @param previousStep Vector of values from previous step.
			/// @return Calculated vector.
			std::vector<double> calculateStep(std::vector<double> previousStep);

		public:
			/// Initializes scheme by configuration parameters.
			///
			/// @param configuration Configuration parameters for simulation.
			explicit ImplicitUpwind(IO::AdvectionConfiguration& configuration);
			
			/// Checks if scheme is stable for internal cfl number.
			///
			/// @return True for stable scheme, false otherwise.
			bool isStable();
		};
	}
}

#endif
