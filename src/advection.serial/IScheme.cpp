///
/// \file IScheme.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Implementation of non-abstract methods.
//////////////////////////////////////////////////////////////////////////

#include "IScheme.h"

#include <cmath>
#include <functional>
#include <stdexcept>

namespace advection
{
	namespace scheme
	{
		// SOLVER
		//////////////////////////////////////////////////////////////////////////

		void IScheme::solve()
		{
			if (!isStable())
				throw std::domain_error("Scheme is unstable for given CFL.");
			currentTime = 0.0;
			prepareContainers();
			std::vector<double> step = initialize();
			for (int tl = 0; tl < timeLevels.size(); tl++)
			{
				while (currentTime <= timeLevels[tl])
				{
					currentTime += deltaTime;
					step = calculateStep(step);
				}
				numericalValues[tl] = step;
				calculateError(tl);
			}
		}

		// INTERNAL
		//////////////////////////////////////////////////////////////////////////

		void IScheme::calculateError(int timeStep)
		{
			std::vector<double> error(gridSize);
			for (int g = 0; g < gridSize; g++)
			{
				double analytical = solveAnalytically(grid[g], currentTime, acceleration);
				analyticalValues[timeStep].push_back(analytical);
				error[g] = analytical - numericalValues[timeStep][g];
			}
			tool::Norms norms = tool::calculateNorms(error);
			errorNorms.push_back(norms);
		}

		void IScheme::prepareContainers()
		{
			errorNorms = std::vector<tool::Norms>();
			grid = std::vector<double>(gridSize);
			numericalValues = std::vector<std::vector<double> >(timeLevels.size());
			analyticalValues = std::vector<std::vector<double> >(timeLevels.size());
			double x = lowerBoundary;
			for (int g = 0; g < gridSize; g++, x += deltaSpace)
			{
				grid[g] = x;
			}
		}

		double IScheme::calculateDeltaTime() const
		{
			return (cfl * deltaSpace) / acceleration;
		}

		std::vector<double> IScheme::initialize()
		{
			std::vector<double> initialStep(gridSize);
			for (int i = 0; i < gridSize; i++)
			{
				initialStep[i] = solveAnalytically(grid[i], currentTime, acceleration);
			}
			return initialStep;
		}

		// CONSTRUCTORS
		//////////////////////////////////////////////////////////////////////////

		IScheme::IScheme(IO::AdvectionConfiguration& configuration)
		{
			currentTime = 0.0;
			cfl = configuration.cfl;
			gridSize = configuration.gridSize;
			acceleration = configuration.acceleration;
			lowerBoundary = configuration.lowerBoundary;
			upperBoundary = configuration.upperBoundary;
			solveAnalytically = configuration.initFunction;
			timeLevels = configuration.timeLevels;
			deltaSpace = (upperBoundary - lowerBoundary) / (gridSize - 1);
			deltaTime = calculateDeltaTime();
		}

		IScheme::~IScheme()
		{
		}

		// ACCESSOR METHODS
		//////////////////////////////////////////////////////////////////////////

		double IScheme::getDeltaSpace() const
		{
			return deltaSpace;
		}

		double IScheme::getDeltaTime() const
		{
			return deltaTime;
		}

		double IScheme::getCFL() const
		{
			return cfl;
		}

		std::vector<double> IScheme::getGrid() const
		{
			return grid;
		}

		std::vector<std::vector<double> > IScheme::getAnalyticalValues() const
		{
			return analyticalValues;
		}

		std::vector<tool::Norms> IScheme::getNorms() const
		{
			return errorNorms;
		}

		std::vector<std::vector<double> > IScheme::getNumericalValues() const
		{
			return numericalValues;
		}
	}
}
