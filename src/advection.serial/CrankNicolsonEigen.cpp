///
/// \file CrankNicolsonEigen.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Implementation of Crank-Nicolson solver.
//////////////////////////////////////////////////////////////////////////

#include "CrankNicolsonEigen.h"
#include "Thomas.h"

namespace advection
{
	namespace scheme
	{
		bool CrankNicolsonEigen::isStable()
		{
			return true;
		}

		std::vector<double> CrankNicolsonEigen::calculateStep(std::vector<double> previousStep)
		{
			Eigen::VectorXd q(gridSize);
			q[0] = previousStep[0] - c * (previousStep[1] - previousStep.back());
			for (int g = 1; g < gridSize - 1; g++)
			{
				q[g] = previousStep[g] - c * (previousStep[g + 1] - previousStep[g - 1]);
			}
			q[gridSize-1] = previousStep.back() - c * (previousStep[0] - previousStep[gridSize - 2]);
			Eigen::VectorXd x = lu.solve(q);
			return std::vector<double>(x.data(), x.data() + x.rows());
		}

		CrankNicolsonEigen::CrankNicolsonEigen(IO::AdvectionConfiguration& configuration)
			: IScheme(configuration)
		{
			c = 0.25 * cfl;
			Eigen::MatrixXd matrix = Eigen::MatrixXd::Zero(gridSize, gridSize);
			(matrix)(0, 0) = 1.0;
			(matrix)(0, 1) = c; 
			int rowPosition = 1;
			for (int i = 0; i < gridSize - 2; i++)
			{
				(matrix)(rowPosition, i) = -c;
				(matrix)(rowPosition, i + 1) = 1.0;
				(matrix)(rowPosition, i + 2) = c;
				rowPosition++;
			}
			(matrix)(gridSize - 1, gridSize - 2) = -c;
			(matrix)(gridSize - 1, gridSize - 1) = 1.0;
			lu = matrix.partialPivLu();
		}
	}
}
