///
/// \mainpage Linear Advection Equation - Serial Numerical Solution
///
/// \section intro_sec Introduction
/// This program solves numerically a linear advection equation, by using
/// numerical schemes discussed in the Computational Methods lectures at
/// Cranfield University, UK. Current implementation supports Explicit and
/// Implicit Upwind scheme, and Crank-Nicolson method. This is serial
/// implementation of program, without MPI.
///
/// \section usage_sec Usage
/// Program takes as parameters following things:
/// - \-config [config_path]
///		Path to configuration file with simulation arguments
/// - \-out [out_file_name]
///		Name of the output file where the results will be saved
///
/// \section config_sec Configuration file
///	Configuration file must contain all of the below parameters.
/// Line starting with `#` is a comment and will be ignored by program.
/// Keys and values should be separated by `=`.
///    <CODE>
///    \n\# Exemplary configuration, order of parameters is not important
///    \n\# [int] Number of points into which program will divide space 
///    \n gridSize=1000
///    \n\# [double] Lower boundary of space domain
///    \n lowerBoundary=-50.0
///    \n\# [double] Upper boundary of space domain
///    \n upperBoundary=50.0
///    \n\# [double] Acceleration parameter for advection equation
///    \n acceleration=1.75
///    \n\# [string] Scheme name, available:
///    \n\# explicit-upwind, implicit-upwind, crank-nicolson
///    \n scheme=explicit-upwind
///    \n\# [string] Name of initialization function, available: 
///    \n\# exponential
///    \n initFunction=exponential
///    \n\# [arr of int] Time levels which will be saved as result of program
///    \n timeLevels=5 10
///    \n\# [double] Courant-Friedrichs-Lewy number for scheme
///    \n cfl=0.999
///    </CODE>
///
/// \section results_sec Results
/// Results of the program are saved into two separate files.
/// First of them is the result itself, that contains data stored in
/// columns:
/// <CENTER>
/// X Analytical(T=t1) Numerical(T=t1) ... Analytical(T=tN) Numerical(T=tN)
/// </CENTER>
///
/// The second file stores the metadata, that is related to the
/// produced results, such as error norms and parameters values.
//////////////////////////////////////////////////////////////////////////
///
/// \file Advection.Serial.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Entry point of application
///
/// Application's main function.
//////////////////////////////////////////////////////////////////////////

//#include <mpi.h>
#include <chrono>
#include "MPITools.h"
#include "InputParser.h"
#include "ConfigurationReader.h"
#include "SchemeFactory.h"
#include "AdvectionConfiguration.h"
#include "ResultFormatter.h"
#include "MetadataFormatter.h"

#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <typeinfo>

//////////////////////////////////////////////////////////////////////////
/// Application's main function.
/// This function describes program flow and is responsible for exception
/// handling and reporting any errors to user.
///
/// @param argc Argument count
/// @param argv Argument vector
/// @return Exit code
int main(int argc, char* argv[])
{
	try
	{
		using namespace advection;

		tool::MPIInfo mpiInfo;

		// Initialize MPI communication protocol
		//MPI_Init(&argc, &argv);
		//MPI_Comm_rank(MPI_COMM_WORLD, &mpiInfo.nodeId);
		//MPI_Comm_size(MPI_COMM_WORLD, &mpiInfo.totalNodes);

		// Get command line arguments
		IO::InputParser parser(argc, argv);
		const std::string configPath = parser.getOption("-config");
		const std::string outputFileBaseName = parser.getOption("-out");

		// Create configuration for advection solver
		IO::ConfigurationReader reader(configPath);
		std::string schemeName = reader.getParameter("scheme");
		IO::AdvectionConfiguration configuration = readConfig(reader);

		// Start simulation timer on master node
		/*double simulationStartTime, simulationEndTime;
		if (tool::isMaster(mpiInfo))
		{
			simulationStartTime = MPI_Wtime();
		}*/
		auto simulationStartTime = std::chrono::steady_clock::now();
		// Solve problem with chosen scheme
		scheme::IScheme* scheme = scheme::SchemeFactory::makeScheme(schemeName, configuration);
		scheme->solve();

		// Stop simulation timer
		/*if (tool::isMaster(mpiInfo))
		{
			simulationEndTime = MPI_Wtime();
		}*/
		auto simulationEndTime = std::chrono::steady_clock::now();
		auto timeSpan = (double)std::chrono::duration_cast<std::chrono::milliseconds>(simulationEndTime - simulationStartTime).count() / 10e3;

		// Save results
		std::stringstream resultFileName;
		resultFileName << outputFileBaseName << ".tsv";
		IO::ResultFormatter results(
			scheme->getGrid(),
			scheme->getAnalyticalValues(),
			scheme->getNumericalValues(),
			configuration.timeLevels,
			resultFileName.str());
		results.save();

		// Save metadata
		std::stringstream metadataFileName;
		metadataFileName << outputFileBaseName << ".meta";
		IO::MetadataFormatter metadata(
			configuration.gridSize,
			scheme->getDeltaTime(),
			scheme->getDeltaSpace(),
			scheme->getNorms(),
			schemeName,
			timeSpan,
			//simulationEndTime - simulationStartTime,
			metadataFileName.str());
		metadata.save();

		// Cleanup and return with normal exit code
		delete scheme;
		//MPI_Finalize();
		return 0;
	}
	catch (std::logic_error& err)
	{
		std::cerr << "Logic error: " << typeid(err).name() << std::endl;
		std::cerr << err.what() << std::endl;
		return -1;
	}
	catch (std::runtime_error& err)
	{
		std::cerr << "Runtime error: " << typeid(err).name() << std::endl;
		std::cerr << err.what() << std::endl;
		return -2;
	}
	catch (std::exception& err)
	{
		std::cerr << "Unknown error: " << typeid(err).name() << std::endl;
		std::cerr << err.what() << std::endl;
		return -3;
	}
}
