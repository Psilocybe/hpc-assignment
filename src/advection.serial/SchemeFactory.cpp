///
/// \file SchemeFactory.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Implementation of scheme factory.
//////////////////////////////////////////////////////////////////////////

#include "SchemeFactory.h"
#include "ExplicitUpwind.h"
#include "ImplicitUpwind.h"
#include "CrankNicolson.h"
#include "CrankNicolsonEigen.h"

#include <stdexcept>

namespace advection
{
	namespace scheme
	{
		IScheme * advection::scheme::SchemeFactory::makeScheme(std::string schemeName, IO::AdvectionConfiguration & configuration)
		{
			if (schemeName == "explicit-upwind")
				return new ExplicitUpwind(configuration);
			if (schemeName == "implicit-upwind")
				return new ImplicitUpwind(configuration);
			if (schemeName == "crank-nicolson")
				return new CrankNicolson(configuration);
			if (schemeName == "crank-nicolson-eigen")
				return new CrankNicolsonEigen(configuration);
			throw std::invalid_argument("Unknown scheme requested.");
		}
	}
}
