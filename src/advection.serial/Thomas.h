///
/// \file Thomas.h
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Thomas algorithm - three diagnoal matrix solver.
///
/// Quick 3-diagonal matrix solver based on Thomas algorithm.
//////////////////////////////////////////////////////////////////////////

#ifndef _THOMAS_H
#define _THOMAS_H

#include <vector>

namespace advection
{
	namespace tool
	{
		/// \brief LU matrix decomposition for Thomas algorithm.
		/// Simplified version for solving the 3-diagonal system [b a c]*X=Q,
		/// with matrices with a single constant value on each of 3 diagonals.
		///
		/// @param lower Value on lower diagonal of original matrix.
		/// @param middle Value on middle diagonal of original matrix.
		/// @param upper Value on upper diagonal of original matrix.
		/// @param l Lower diagonal result of LU decomposition.
		/// @param d Middle diagonal result of LU decomposition.
		/// @param u Upper diagonal result of LU decomposition.
		void ThomasAlgorithmLU(
			double lower, double middle, double upper,
			std::vector<double>& l, std::vector<double>& d, std::vector<double>& u);

		/// \brief LU matrix solver for Thomas algorithm.
		/// Simplified version for solving the 3-diagonal system [b a c]*X=Q,
		/// with matrices with a single constant value on each of 3 diagonals.
		///
		/// @param l Value of lower diagonal of LU matrix.
		/// @param d Value of middle diagonal of LU matrix.
		/// @param u Value of upper diagonal of LU matrix.
		/// @param q Vector Q of constant terms.
		/// @return Solution vector X.
		std::vector<double> ThomasAlgorithmSolve(
			const std::vector<double>& l, const std::vector<double>& d,
			const std::vector<double>& u, const std::vector<double>& q);
	}
}

#endif
