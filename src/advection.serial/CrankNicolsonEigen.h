///
/// \file CrankNicolsonEigen.h
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Declaration of advection solver based on Crank-Nicolson method.
//////////////////////////////////////////////////////////////////////////

#ifndef _CRANK_NICOLSON_EIGEN_H
#define _CRANK_NICOLSON_EIGEN_H

#include "IScheme.h"

#include <vector>
#include <Eigen>

namespace advection
{
	namespace scheme
	{
		/// Advection equation solver implementing Crank-Nicolson method with help of Eigen library.
		class CrankNicolsonEigen
			: public IScheme
		{
			/// Constant value based on CFL.
			double c;

			/// Eigen LU matrices.
			Eigen::PartialPivLU<Eigen::MatrixXd> lu;

		protected:
			/// Calculates values for next time step.
			///
			/// @param previousStep Vector of values from previous step.
			/// @return Calculated vector.
			std::vector<double> calculateStep(std::vector<double> previousStep);
			
		public:
			/// Initializes scheme by configuration parameters.
			///
			/// @param configuration Configuration parameters for simulation.
			explicit CrankNicolsonEigen(IO::AdvectionConfiguration &configuration);

			/// Checks if scheme is stable for internal cfl number.
			///
			/// @return True for stable scheme, false otherwise.
			bool isStable();
		};
	}
}

#endif
