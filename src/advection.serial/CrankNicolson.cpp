///
/// \file CrankNicolson.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Implementation of Crank-Nicolson solver.
//////////////////////////////////////////////////////////////////////////

#include "CrankNicolson.h"
#include "Thomas.h"

namespace advection
{
	namespace scheme
	{
		bool CrankNicolson::isStable()
		{
			return true;
		}

		std::vector<double> CrankNicolson::calculateStep(std::vector<double> previousStep)
		{
			std::vector<double> currentStep(gridSize);
			currentStep[0] = previousStep[0] - c * (previousStep[1] - previousStep.back());
			for (int g = 1; g < gridSize - 1; g++)
			{
				currentStep[g] = previousStep[g] - c * (previousStep[g + 1] - previousStep[g - 1]);
			}
			currentStep.back() = previousStep.back() - c * (previousStep[0] - previousStep[gridSize - 2]);
			return tool::ThomasAlgorithmSolve(lower, middle, upper, currentStep);
		}

		CrankNicolson::CrankNicolson(IO::AdvectionConfiguration& configuration)
			: IScheme(configuration)
		{
			c = 0.25 * cfl;
			lower = std::vector<double>(gridSize);
			middle = std::vector<double>(gridSize);
			upper = std::vector<double>(gridSize);
			tool::ThomasAlgorithmLU(-c, 1.0, c, lower, middle, upper);
		}
	}
}
