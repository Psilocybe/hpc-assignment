///
/// \file ExplicitUpwind.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Implementation of explicit upwind solver.
//////////////////////////////////////////////////////////////////////////

#include "ExplicitUpwind.h"

namespace advection
{
	namespace scheme
	{
		bool ExplicitUpwind::isStable()
		{
			return cfl <= 1.0;
		}

		std::vector<double> ExplicitUpwind::calculateStep(std::vector<double> previousStep)
		{
			std::vector<double> currentStep(gridSize);
			currentStep[0] = previousStep[0];
			for (int g = 1; g < gridSize - 1; g++)
			{
				currentStep[g] = previousStep[g] + cfl * (previousStep[g - 1] - previousStep[g]);
			}
			currentStep[gridSize - 1] = previousStep[gridSize - 1];
			return currentStep;
		}

		ExplicitUpwind::ExplicitUpwind(IO::AdvectionConfiguration& configuration)
			: IScheme(configuration)
		{
		}
	}
}
