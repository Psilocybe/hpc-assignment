///
/// \file CrankNicolson.h
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Declaration of advection solver based on Crank-Nicolson method.
//////////////////////////////////////////////////////////////////////////

#ifndef _CRANK_NICOLSON_H
#define _CRANK_NICOLSON_H

#include "IScheme.h"

#include <vector>

namespace advection
{
	namespace scheme
	{
		/// Advection equation solver implementing Crank-Nicolson method.
		class CrankNicolson
			: public IScheme
		{
			/// Constant value based on CFL.
			double c;

			/// Lower diagonal of LU matrix.
			std::vector<double> lower;

			/// Middle diagonal of LU matrix.
			std::vector<double> middle;

			/// Upper diagonal of LU matrix.
			std::vector<double> upper;
			
		protected:
			/// Calculates values for next time step.
			///
			/// @param previousStep Vector of values from previous step.
			/// @return Calculated vector.
			std::vector<double> calculateStep(std::vector<double> previousStep);

		public:
			/// Initializes scheme by configuration parameters.
			///
			/// @param configuration Configuration parameters for simulation.
			explicit CrankNicolson(IO::AdvectionConfiguration &configuration);

			/// Checks if scheme is stable for internal cfl number.
			///
			/// @return True for stable scheme, false otherwise.
			bool isStable();
		};
	}
}

#endif
