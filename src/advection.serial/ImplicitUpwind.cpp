///
/// \file ImplicitUpwind.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Implementation of implicit upwind solver.
//////////////////////////////////////////////////////////////////////////

#include "ImplicitUpwind.h"

namespace advection
{
	namespace scheme
	{
		bool ImplicitUpwind::isStable()
		{
			return 0 <= cfl;
		}

		void ImplicitUpwind::forward(std::vector<double>& currentStep, const std::vector<double>& previousStep) const
		{
			for (int g = 1; g < gridSize; g++)
			{
				currentStep[g] = (previousStep[g] + cfl * currentStep[g - 1]) / (1.0 + cfl);
			}
		}

		std::vector<double> ImplicitUpwind::calculateStep(std::vector<double> previousStep)
		{
			std::vector<double> currentStep(gridSize);
			currentStep[0] = previousStep[0];
			currentStep[gridSize - 1] = previousStep[gridSize - 1];
			forward(currentStep, previousStep); 
			return currentStep;
		}

		ImplicitUpwind::ImplicitUpwind(IO::AdvectionConfiguration& configuration)
			: IScheme(configuration)
		{
		}
	}
}
