///
/// \file Thomas.cpp
///
/// \author Bartlomiej Szostek
/// \date Jaunuary 2017
///
/// \brief Implementation of Thomas algorithm.
///
/// Implementations of Thomas 3-diagonal matrix solver.
//////////////////////////////////////////////////////////////////////////

#include "Thomas.h"

#include <cmath>

namespace advection
{
	namespace tool
	{
		void ThomasAlgorithmLU(
			double lower, double middle, double upper,
			std::vector<double>& l, std::vector<double>& d, std::vector<double>& u)
		{
			int size = d.size();
			d[0] = middle;
			u[0] = upper;
			for (int i = 0; i < size - 1; i++)
			{
				l[i] = lower / d[i];
				d[i + 1] = middle - l[i] * u[i];
				u[i + 1] = upper;
			}
			l[size - 1] = lower / d[size - 1];
		}

		std::vector<double> ThomasAlgorithmSolve(
			const std::vector<double>& l, const std::vector<double>& d,
			const std::vector<double>& u, const std::vector<double>& q)
		{
			int size = q.size();
			std::vector<double> result(size), y(size);
			// Forward Substitution [L][y] = [q]
			y[0] = q[0];
			for (int i = 1; i < size; i++)
			{
				y[i] = q[i] - l[i - 1] * y[i - 1];
			}
			// Backward Substitution [U][x] = [y]
			result[size - 1] = y[size - 1] / d[size - 1];
			for (int i = size - 2; i >= 0; i--)
			{
				result[i] = (y[i] - u[i] * result[i + 1]) / d[i];
			}
			return result;
		}
	}
}
