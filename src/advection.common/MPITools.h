///
/// \file MPITools.h
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Declaration of helper functions for MPI.
//////////////////////////////////////////////////////////////////////////

#ifndef _MPI_TOOLS_H
#define _MPI_TOOLS_H

namespace advection
{
	namespace tool
	{
		/// Keeps information about MPI working environment.
		struct MPIInfo
		{
			int nodeId; /*!< Current node identifier. */
			int totalNodes; /*!< Total number of working nodes. */
		};

		/// MPI messages tags.
		enum MPITags
		{
			BOUNDARY_VALUE = 0, /*!< Tag for exchanging scheme boundary values. */
			NUMERICAL_VALUES = 1, /*!< Tag for passing final numerical values. */
			GRID_VALUES = 2, /*!< Tag for sending grid points. */
			THOMAS_DISTRIBUTE = 3, /*!< Tag for distributing values in parallel Thomas algorithm. */
			THOMAS_DISTRIBUTE_X = 4, /*!< Tag for distributing X values in parallel Thomas algorithm solver. */
			THOMAS_DISTRIBUTE_Y = 5, /*!< Tag for distributing Y values in parallel Thomas algorithm solver. */
			CRANK_LEFT = 6, /*!< Tag for distributing left boundary values in Crank-Nicolson method. */
			CRANK_RIGHT = 7 /*!< Tag for distributing right boundary values in Crank-Nicolson method. */
		};

		/// Checks if current node is master node.
		///
		/// @param mpiInfo Informations about current MPI configuration.
		/// @return True, if current node is master, false otherwise.
		bool isMaster(const MPIInfo& mpiInfo);

		/// Checks if current node is last node.
		///
		/// @param mpiInfo Informations about current MPI configuration.
		/// @return True, if current node is the last one, false otherwise.
		bool isLastNode(const MPIInfo& mpiInfo);

		/// Calculates the index in global vector for current node based on node identifier and total size of system.
		///
		/// @param mpiInfo Informations about current MPI configuration.
		/// @param vectorSize Total size of the vector.
		/// @param localIndex Index of local array.
		/// @return Size of the vector for given node identifier.
		int getIndexInGlobalVector(const MPIInfo& mpiInfo, int vectorSize, int localIndex);

		/// Calculates the size of local vector based on node identifier and total size of system.
		///
		/// @param mpiInfo Informations about current MPI configuration.
		/// @param vectorSize Total size of the vector.
		/// @return Size of the vector for given node identifier.
		int getLocalVectorSize(const MPIInfo& mpiInfo, int vectorSize);
	}
}

#endif
