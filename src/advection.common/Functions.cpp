///
/// \file Functions.cpp
///
/// \author Bartlomiej Szostek
/// \date Jaunuary 2017
///
/// \brief Implementation of analytical solutions for advection equation.
///
/// Implementations of functions that are solving analytically advection
/// equation and internally used functions.
//////////////////////////////////////////////////////////////////////////

#include "Functions.h"

#include <cmath>

namespace advection
{
	namespace analytical
	{
		double exponentialFunction(const double x, const double time, const double acceleration)
		{
			double abscissa = x - acceleration * time;
			return 0.5 * exp(-1.0 * abscissa * abscissa);
		}
	}
}

