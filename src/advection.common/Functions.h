///
/// \file Functions.h
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Analytical solutions for advection equation.
///
/// Functions that are solving analytically advection equation.
//////////////////////////////////////////////////////////////////////////

#ifndef _FUNCTIONS_H
#define _FUNCTIONS_H

#include <type_traits>

/// Global namespace common to all files.
namespace advection
{
	/// Encapsulates analytical solutions for advection equation.
	namespace analytical
	{
		/// Analytical solution for advection equation based on exponential function.
		/// Calculates value of function: \f$0.5 * exp(-(x - u*t)^2)\f$
		///
		/// @param x Point in space.
		/// @param time Current time.
		/// @param acceleration Acceleration of wave.
		/// @return Value of exponential based analytical solution.
		double exponentialFunction(const double x, const double time, const double acceleration);
	}
}

#endif

