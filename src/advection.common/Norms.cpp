///
/// \file Norms.h
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Implementation of vector norms.
//////////////////////////////////////////////////////////////////////////

#include "Norms.h"

#include <cmath>

namespace advection
{
	namespace tool
	{
		double oneNorm(const std::vector<double> vec)
		{
			double sum = 0.0;
			for (int i = 0; i < vec.size(); i++)
			{
				sum += fabs(vec[i]);
			}
			return sum / vec.size();
		}

		double twoNorm(const std::vector<double> vec)
		{
			double sum = 0.0;
			for (int i = 0; i < vec.size(); i++)
			{
				sum += vec[i] * vec[i];
			}
			return sqrt(sum);
		}

		double uniformNorm(const std::vector<double> vec)
		{
			double max = fabs(vec[0]);
			for (int i = 0; i < vec.size(); i++)
			{
				double current = fabs(vec[i]);
				if (max < current)
					max = current;
			}
			return max;
		}

		Norms calculateNorms(const std::vector<double> vec)
		{
			if (vec.size() == 0)
				throw std::logic_error("Cannot calculate norm on zero vector.");
			Norms norms;
			norms.one = oneNorm(vec);
			norms.two = twoNorm(vec);
			norms.uniform = uniformNorm(vec);
			return norms;
		}
	}
}
