///
/// \file MetadataFormatter.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Methods implementation of metadata formatter.
//////////////////////////////////////////////////////////////////////////

#include "MetadataFormatter.h"

namespace advection
{
	namespace IO
	{
		MetadataFormatter::MetadataFormatter(
			int gridSize,
			double deltaTime,
			double deltaSpace,
			std::vector<tool::Norms> norms,
			std::string scheme,
			double simulationExecutionTime,
			std::string fileName)
			: IFormatter(fileName),
			scheme(scheme), gridSize(gridSize), deltaTime(deltaTime), deltaSpace(deltaSpace),
			norms(norms), simulationExecutionTime(simulationExecutionTime)
		{
		}

		void MetadataFormatter::save()
		{
			outputFile << "Execution time [s]= " << simulationExecutionTime << std::endl;
			outputFile << "Scheme= " << scheme << std::endl;
			outputFile << "Delta(t)= " << deltaTime << std::endl;
			outputFile << "Delta(x)= " << deltaSpace << std::endl;
			outputFile << "Grid size= " << gridSize << std::endl;
			double sumOne = 0.0, sumTwo = 0.0, sumUniform = 0.0;
			for (int i = 0; i < norms.size(); ++i)
			{
				outputFile << "(T=" << i << ") Norm 1= " << norms.at(i).one << std::endl;
				outputFile << "(T=" << i << ") Norm 2= " << norms.at(i).two << std::endl;
				outputFile << "(T=" << i << ") Uniform norm= " << norms.at(i).uniform << std::endl;
				sumOne += norms.at(i).one; sumTwo += norms.at(i).two; sumUniform += norms.at(i).uniform;
			}
			outputFile << "Average norm 1= " << sumOne / norms.size() << std::endl;
			outputFile << "Average norm 2= " << sumTwo / norms.size() << std::endl;
			outputFile << "Average uniform norm= " << sumUniform / norms.size() << std::endl;
		}
	}
}
