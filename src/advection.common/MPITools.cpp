///
/// \file MPITools.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Implementation of helper functions for MPI.
//////////////////////////////////////////////////////////////////////////

#include "MPITools.h"

namespace advection
{
	namespace tool
	{
		bool isMaster(const MPIInfo& mpiInfo)
		{
			return (mpiInfo.nodeId == 0);
		}

		bool isLastNode(const MPIInfo& mpiInfo)
		{
			return (mpiInfo.nodeId == (mpiInfo.totalNodes - 1));
		}

		int getIndexInGlobalVector(const MPIInfo& mpiInfo, int vectorSize, int localIndex)
		{
			return (mpiInfo.nodeId * vectorSize / mpiInfo.totalNodes) + localIndex;
		}

		int getLocalVectorSize(const MPIInfo& mpiInfo, int vectorSize)
		{
			int notAssignedPoints = vectorSize % mpiInfo.totalNodes;
			if (notAssignedPoints == 0)
			{
				return (vectorSize / mpiInfo.totalNodes);
			}
			else if (!isLastNode(mpiInfo))
			{
				return ((vectorSize - notAssignedPoints) / mpiInfo.totalNodes);
			}
			else
			{
				return (((vectorSize - notAssignedPoints) / mpiInfo.totalNodes) + notAssignedPoints);
			}
		}
	}
}
