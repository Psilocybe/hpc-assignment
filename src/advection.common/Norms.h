///
/// \file Norms.h
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Contains tools to calculate norms of vectors.
//////////////////////////////////////////////////////////////////////////

#ifndef _NORMS_H
#define _NORMS_H

#include <cmath>
#include <numeric>
#include <stdexcept>
#include <vector>

namespace advection
{
	namespace tool
	{
		/// Keeps information about all norms in one place.
		struct Norms
		{
			double one; /*!< Value of 1 norm. */
			double two; /*!< Value of 2 norm. */
			double uniform; /*!< Value of uniform (infinity) norm. */
		};

		/// Calculates 1 norm of vector.
		///
		/// @param vec Standard vector to calculate his 1 norm.
		/// @return Norm 1 of given vector.
		double oneNorm(const std::vector<double> vec);

		/// Calculates 2 norm of vector.
		///
		/// @param vec Standard vector to calculate his 2 norm.
		/// @return Norm 2 of given vector.
		double twoNorm(const std::vector<double> vec);

		/// Calculates uniform (infinity) norm of vector.
		///
		/// @param vec Standard vector to calculate his uniform norm.
		/// @return Uniform norm of given vector.
		double uniformNorm(const std::vector<double> vec);

		/// Calculate all norms for given vector.
		///
		/// @param vec Standard vector to calculate his norms.
		/// @return Structure containing all calculated norms.
		/// @throws logic_error When vector size is equal to 0.
		Norms calculateNorms(const std::vector<double> vec);
	}
}

#endif
