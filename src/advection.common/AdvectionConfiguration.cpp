///
/// \file AdvectionConfiguration.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Implementation of configuration parser.
///
/// Implementations of functions that are parsing configuration files
/// provided by user. Each parameter is read from config reader and 
/// assigned to proper AdvectionConfiguration fields.
//////////////////////////////////////////////////////////////////////////

#include "AdvectionConfiguration.h"
#include "ConfigurationReader.h"
#include "Functions.h"

#include <stdexcept>
#include <string>

namespace advection
{
	namespace IO
	{
		AdvectionConfiguration readConfig(ConfigurationReader& reader)
		{
			AdvectionConfiguration config;
			config.cfl = reader.getDouble("cfl");
			config.gridSize = reader.getInteger("gridSize");
			config.lowerBoundary = reader.getDouble("lowerBoundary");
			config.upperBoundary = reader.getDouble("upperBoundary");
			config.acceleration = reader.getDouble("acceleration");
			std::string initFunction = reader.getParameter("initFunction");
			if (initFunction.compare("exponential") == 0)
				config.initFunction = &analytical::exponentialFunction;
			else
				throw std::invalid_argument("Unknown initialization function name.");
			std::vector<double> timeLevels = reader.getDoubleVector("timeLevels");
			config.timeLevels = timeLevels;
			return config;
		}
	}
}

