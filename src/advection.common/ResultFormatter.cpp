///
/// \file ResultFormatter.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Methods implementation of result formatter.
//////////////////////////////////////////////////////////////////////////

#include "ResultFormatter.h"

#include <iomanip>
#include <stdexcept>
#include <string>

namespace advection
{
	namespace IO
	{
		void ResultFormatter::saveValues(int position)
		{
			outputFile << std::scientific << std::setprecision(resultPrecision);
			for (int i = 0; i < analyticalValues.size(); ++i)
			{
				outputFile << analyticalValues[i][position] << delimiter;
				outputFile << numericalValues[i][position] << delimiter;
			}
		}

		ResultFormatter::ResultFormatter(
			std::vector<double> grid,
			std::vector<std::vector<double> > analyticalValues,
			std::vector<std::vector<double> > numericalValues,
			std::vector<double> timeLevels,
			const std::string fileName)
			: IFormatter(fileName),
			grid(grid), timeLevels(timeLevels), numericalValues(numericalValues), analyticalValues(analyticalValues)
		{
			if (timeLevels.size() != numericalValues.size() ||
				timeLevels.size() != analyticalValues.size() ||
				numericalValues.size() != analyticalValues.size())
				throw std::invalid_argument("Vector sizes should match.");
			delimiter = '\t';
			resultPrecision = 6;
		}

		void ResultFormatter::save()
		{
			for (int i = 0; i < grid.size(); i++)
			{
				outputFile << std::fixed;
				outputFile << std::setprecision(resultPrecision) << grid.at(i) << delimiter;
				saveValues(i);
				outputFile << std::endl;
			}
		}
	}
}
