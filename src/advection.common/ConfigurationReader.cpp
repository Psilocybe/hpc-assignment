///
/// \file ConfigurationReader.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Implementation of configuration reader.
//////////////////////////////////////////////////////////////////////////

#include "ConfigurationReader.h"

#include <algorithm>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <sstream>
#include <stdexcept>

namespace advection
{
	namespace IO
	{
		void ConfigurationReader::readConfiguration(std::istream &stream)
		{
			commentCharacter = '#';
			keyValueDelimiter = '=';
			std::string line;
			while (std::getline(stream, line))
			{
				if (line[0] == commentCharacter) continue; //skip line of comment
				std::istringstream is_line(line);
				std::string key;
				if (std::getline(is_line, key, keyValueDelimiter))
				{
					std::string value;
					if (std::getline(is_line, value))
					{
						key = makeKey(key);
						if (options.count(key))
							throw std::logic_error("Duplicate configuration keys: " + key + ".");
						options.insert(std::make_pair(key, value));
					}
				}
			}
		}

		std::string ConfigurationReader::makeKey(std::string key) const
		{
			std::transform(key.begin(), key.end(), key.begin(), ::tolower);
			return key;
		}

		ConfigurationReader::ConfigurationReader(const std::string &configFileName)
		{
			std::ifstream configStream(configFileName.c_str());
			if (!configStream.is_open())
				throw std::runtime_error("Unable to open configuration file.");
			readConfiguration(configStream);
		}

		ConfigurationReader::ConfigurationReader(std::istream &stream)
		{
			readConfiguration(stream);
		}

		bool ConfigurationReader::parameterExists(const std::string& key) const
		{
			return options.count(makeKey(key)) > 0;
		}

		std::string ConfigurationReader::getParameter(const std::string key) const
		{
			try
			{
				return options.at(makeKey(key));
			}
			catch (const std::out_of_range&)
			{
				throw std::out_of_range("Configuration key not found: " + key + ".");
			}
		}

		int ConfigurationReader::getInteger(const std::string & key) const
		{
			const std::string value = getParameter(key);
			return std::atoi(value.c_str());
		}

		double ConfigurationReader::getDouble(const std::string& key) const
		{
			const std::string value = getParameter(key);
			return std::atof(value.c_str());
		}
		std::vector<double> ConfigurationReader::getDoubleVector(const std::string & key) const
		{
			const std::string value = getParameter(key);
			std::stringstream ss(value);
			std::istream_iterator<std::string> begin(ss);
			std::istream_iterator<std::string> end;
			std::vector<std::string> vstrings(begin, end);
			std::vector<double> parameters(vstrings.size());
			for (int i = 0; i < vstrings.size(); i++)
			{
				parameters[i] = std::atof(vstrings[i].c_str());
			}
			return parameters;
		}
	}
}
