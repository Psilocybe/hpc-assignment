///
/// \file IFormatter.cpp
///
/// \author Bartlomiej Szostek
/// \date January 2017
///
/// \brief Implementation of non abstract output formatter methods.
//////////////////////////////////////////////////////////////////////////

#include "IFormatter.h"

namespace advection
{
	namespace IO
	{
		IFormatter::IFormatter(std::string fileName)
			: outputFile(fileName.c_str())
		{
			outputFile.open(fileName.c_str(), std::fstream::out | std::fstream::trunc);
		}

		IFormatter::~IFormatter()
		{
			outputFile.close();
		}
	}
}
