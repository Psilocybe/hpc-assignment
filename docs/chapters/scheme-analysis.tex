\chapter{Mathematical Analysis of Schemes}
\label{ch:scheme-analysis}

This chapter presents schemes used to solve hyperbolic \glspl{PDE} by utilizing finite difference approximations. In each section, dependencies between concurrent time iterations are shown using geometrical presentation of neighbour grid points. To unify the \emph{\glspl{stencil}}, we assume following notation:
\begin{enumerate}
	\item Nodes with green crosshatch pattern indicate currently calculated point $f_{i}^{n+1}$.
	\item Nodes with blue diagonal lines indicate points, that scheme takes into account for calculations of current point.
	\item Each horizontal line separates time levels, and under each column of nodes there is corresponding relative index in the grid.
	\item Arrows show the direction of the points relations.
\end{enumerate}

Every scheme formula contains reference to the dimensionless number called \gls{CFL} number, or just Courant number. According to Moin \cite{moin}, it is equal to

\begin{equation}\label{eq:courant}
	C=u\frac{\Delta t}{\Delta x}
\end{equation}

In order to produce stable and meaningful solution, each scheme has to meet the \gls{CFL} condition. In one-dimensional advection problem, it is in the following form $C \le C_{max}$, where $C_{max}$ varies between each scheme. Some schemes are unconditionally stable, i.e. they produce stable results no matter what \gls{CFL} is chosen.

\section{Explicit Upwind Scheme}
\label{sec:schemes:explicit}

Using backward differencing in space and forward differencing in time leads to first-order explicit upwind scheme. Formula \eqref{eq:explicitupwind} is given in Hoffman and Chiang \cite[p. 191 - 192]{hoffmann}:

\begin{equation}
	\label{eq:explicitupwind}
	f_{i}^{n+1} = f_{i}^{n} - C(f_{i}^{n}-f_{i-1}^{n})
\end{equation}

Graphical representation of this scheme is presented in figure \ref{fig:explicit-stencil}. Explicit upwind scheme is first order accurate in time and space $\mathcal{O}(\Delta t, \Delta x)$.

\begin{figure}
	\centering
	\begin{tikzpicture}
	\draw[dashed] (-4,1.2) -- (4,1.2);
	\node[black] at (-4.5,0.5) {$n+1$};
	\draw[dashed] (-4,-0.2) -- (4,-0.2);
	\node[black] at (-4.5,-0.9) {$n$};
	\draw[dashed] (-4,-1.6) -- (4,-1.6);
	
	\node[black] at (-1.5,-2) {$i-1$};
	\node[black] at (0,-2) {$i$};
	\node[black] at (+1.5,-2) {$i+1$};
	
	\stencilpt[gray,pattern=dots, pattern color=gray]{-1.5,0.5}{i-1}{};
	\stencilpt[green,pattern=crosshatch, pattern color=green]{0,0.5}{i}{};
	\stencilpt[gray,pattern=dots, pattern color=gray]{+1.5,0.5}{i+1}{};
	\stencilpt[pattern=north west lines, pattern color=blue]{-1.5,-0.9}{j-1}{};
	\stencilpt[pattern=north west lines, pattern color=blue]{0,-0.9}{j}{};
	\stencilpt[gray,pattern=dots, pattern color=gray]{+1.5,-0.9}{j+1}{};
	
	\draw[line width=0.5mm,->,>=stealth] (j-1)--(i);
	\draw[line width=0.5mm,->,>=stealth] (j)--(i);
	\end{tikzpicture}
	\caption{Stencil for explicit upwind scheme.}
	\label{fig:explicit-stencil}
\end{figure}

Stability of the scheme can be determined by using Von Neumann stability analysis, as shown in Collis \cite{collis}. The CFL condition for explicit upwind scheme is: $C \le C_{max} = 1.0$.

\section{Implicit Upwind Scheme}
\label{sec:schemes:implicit}

First-order backward difference both in time and space of \eqref{eq:advection} leads to formula \eqref{eq:implicitupwind}. This method is first order accurate in time and space $\mathcal{O}(\Delta t, \Delta x)$.

\begin{equation}
	\label{eq:implicitupwind}
	f_{i}^{n+1} = f_{i}^{n} - C(f_{i}^{n+1}-f_{i-1}^{n+1})
\end{equation}

To solve equation \eqref{eq:implicitupwind} we have to solve the system of linear equations of form $AX=B$. In this case, \emph{A} is diagonally-dominant matrix of size $N\times N$, where \emph{N} is equal to number of grid points. \emph{X} is the vector of unknowns at current step of simulation, \emph{B} is equal to calculated vector from previous step. To calculate the first step, we substitute the vector \emph{B} with the values obtained from initial condition \ref{eq:initExp} for $t = 0$. Applying equation \ref{eq:implicitupwind} to all grid points, we obtain lower bidiagonal matrix \emph{A} as in Hoffman and Chiang \cite{hoffmann}. A general system of linear equations for \eqref{eq:implicitupwind} of size \emph{N} is shown in \eqref{eq:implicitmatrix}.

\begin{equation}
\label{eq:implicitmatrix}
\begin{bmatrix}
1+C           &        &        & \bigzeroup \\ 
-C            & 1+C    &        &    \\ 
              & \ddots & \ddots &       \\ 
\bigzerodown &        &     -C & 1+C   
\end{bmatrix}
\times 
\begin{bmatrix}
f_{1} \\ f_{2} \\[4pt] \vdots \\[4pt] f_{N}
\end{bmatrix}^{n+1}
=
\begin{bmatrix}
f_{1} + Cf_{0}^{n+1} \\ f_{2} \\[4pt] \vdots \\[4pt] f_{N}
\end{bmatrix}^{n}
\end{equation}

Presented system of equations can simplified by using forward substitution. In the case, when speed of advection wave is positive, i.e. $u > 0$, linearised equation takes the following form \eqref{eq:implicitlinear}.

\begin{equation}
\label{eq:implicitlinear}
f_{i}^{n+1}=\frac{f_{i}^{n}+Cf_{i-1}^{n+1}}{1+C},\quad for \quad i=1,2,3,...,N-1
\end{equation}

From \gls{stencil} \ref{fig:implicit-stencil} we can notice, that currently calculated point relays on value of previous grid point of the same time level, and value of point in previous time iteration. First wave can be solved, by using boundary values $f_{0}^{n+1}$ and $f_{N}^{n+1}$. Von Neumann stability analysis for this scheme shows, that it is unconditionally stable for $C \in (0,\infty)$.
\begin{figure}[ht]
	\centering
	\begin{tikzpicture}
	\draw[dashed] (-4,1.2) -- (4,1.2);
	\node[black] at (-4.5,0.5) {$n+1$};
	\draw[dashed] (-4,-0.2) -- (4,-0.2);
	\node[black] at (-4.5,-0.9) {$n$};
	\draw[dashed] (-4,-1.6) -- (4,-1.6);
	
	\node[black] at (-1.5,-2) {$i-1$};
	\node[black] at (0,-2) {$i$};
	\node[black] at (+1.5,-2) {$i+1$};
	
	\stencilpt[pattern=north west lines, pattern color=blue]{-1.5,0.5}{i-1}{};
	\stencilpt[green,pattern=crosshatch, pattern color=green]{0,0.5}{i}{};
	\stencilpt[gray,pattern=dots, pattern color=gray]{+1.5,0.5}{i+1}{};
	\stencilpt[gray,pattern=dots, pattern color=gray]{-1.5,-0.9}{j-1}{};
	\stencilpt[pattern=north west lines, pattern color=blue]{0,-0.9}{j}{};
	\stencilpt[gray,pattern=dots, pattern color=gray]{+1.5,-0.9}{j+1}{};
	
	\draw[line width=0.5mm,->,>=stealth] (i-1)--(i);
	\draw[line width=0.5mm,->,>=stealth] (j)--(i);
	\end{tikzpicture}
	\caption{Stencil for forward substitution Implicit Upwind scheme.}
	\label{fig:implicit-stencil}
\end{figure}

\section{Crank--Nicolson Method}
\label{sec:schemes:crank}

Crank--Nicolson method for equation \eqref{eq:advection} can be described, by simplifying formulations given by Collis \cite{collis}, Karniadakis and  Kirby in \cite{karniadakis}, as simple as:
\begin{equation}
\label{eq:crank-nicolson:solution}
\frac{C}{4} f_{i-1}^n + f_{i}^n - \frac{C}{4} f_{i+1}^n = -\frac{C}{4} f_{i-1}^{n+1} + f_{i}^{n+1} + \frac{C}{4} f_{i+1}^{n+1}
\end{equation}
where $C$ is \gls{CFL} equal to \eqref{eq:courant}. Separating terms of different time levels in \eqref{eq:crank-nicolson:solution}, leads to system of linear equations:
\begin{equation}
\label{eq:crank-nicolson:dependencies-matrix}
f_i^{n+1} = c f_{i-1}^n + f_{i}^n - c f_{i+1}^n\text{, where } c = \frac{C}{4}
\end{equation}

Because of the dependencies on points on the lower and upper diagonal, this implicit equation \ref{eq:crank-nicolson:solution} cannot be solved as easily as Implicit Upwind Scheme described in section \ref{sec:schemes:implicit}. General form for Crank--Nicolson formula for \emph{N}-sized problem is given in equation \eqref{eq:crank-nicolson:matrix}.
\begin{equation}
	\label{eq:crank-nicolson:matrix}
	\begin{bmatrix}
	1 		     & $c$    &		   &        & \bigzeroup &\\
	$-c$         & 1 	  & $c$    &        &            &\\ 
	             & \ddots & \ddots & \ddots & 		     &\\
	             &        & $-c$   & 1 	    & $c$        &\\
	\bigzerodown &        & 	   & $-c$   &	1	     &\\					
	\end{bmatrix} 
	\times
	\begin{bmatrix}
		f_1 \\ f_2 \\ \\ \vdots	\\ \\ f_{N}\\
	\end{bmatrix}^{n+1}
	=
	\begin{bmatrix}
		f_0 \\ f_1 \\ \\ \vdots	\\ \\ f_{N} \\
	\end{bmatrix}^{n}
\end{equation}

\Gls{stencil} for Crank--Nicolson is shown in figure \ref{fig:stencil:crank-nicolson}. This scheme is first-order accurate both in space and time. This method is unconditionally stable for: $C>0$.
\begin{figure}[ht]
	\centering
	\begin{tikzpicture}
	\draw[dashed] (-4,1.2) -- (4,1.2);
	\node[black] at (-4.5,0.5) {$n+1$};
	\draw[dashed] (-4,-0.2) -- (4,-0.2);
	\node[black] at (-4.5,-0.9) {$n$};
	\draw[dashed] (-4,-1.6) -- (4,-1.6);
	
	\node[black] at (-1.5,-2) {$i-1$};
	\node[black] at (0,-2) {$i$};
	\node[black] at (+1.5,-2) {$i+1$};
	
	\stencilpt[pattern=north west lines, pattern color=blue]{-1.5,0.5}{i-1}{};
	\stencilpt[green,pattern=crosshatch, pattern color=green]{0,0.5}{i}{};
	\stencilpt[pattern=north west lines, pattern color=blue]{+1.5,0.5}{i+1}{};
	\stencilpt[pattern=north west lines, pattern color=blue]{-1.5,-0.9}{j-1}{};
	\stencilpt[pattern=north west lines, pattern color=blue]{0,-0.9}{j}{};
	\stencilpt[pattern=north west lines, pattern color=blue]{+1.5,-0.9}{j+1}{};
	
	\draw[line width=0.5mm,->,>=stealth] (i-1)--(i);
	\draw[line width=0.5mm,->,>=stealth] (i+1)--(i);
	\draw[line width=0.5mm,->,>=stealth] (j-1)--(i);
	\draw[line width=0.5mm,->,>=stealth] (j)--(i);
	\draw[line width=0.5mm,->,>=stealth] (j+1)--(i);
	\end{tikzpicture}
	\caption{Stencil for Crank--Nicolson method.}
	\label{fig:stencil:crank-nicolson}
\end{figure}
