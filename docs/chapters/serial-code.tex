\chapter{Serial Code}
\label{ch:serial-code}

Solving \gls{PDE} with finite difference method can be divided into a few steps. The first thing to do, is to discretize a continuous domain into a (uniform) grid. Next step is to replace the derivatives in the \gls{PDE} with equivalent finite difference expressions. Final step consists of rearranging the terms of obtained equation to an algorithm, that can be later easily implemented using common programming languages. Fletcher explains this process in his book Computational Techniques for Fluid Dynamics \cite{fletcher}. Based on this schema, we introduce more flexible version of general approach to programs solving finite differences in figure \ref{fig:finite-diff-prog-schema}.
	
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=\textwidth]{finite-diff-prog-schema}
	\caption[Schematic of the finite difference solution process.]{Schematic of the finite difference solution process.\footnotemark}
	\label{fig:finite-diff-prog-schema}
\end{figure}
\footnotetext{See \cite[p.~65]{fletcher}}

This general approach can be specialized in solving certain problem, by transforming it's \gls{PDE} terms using chosen scheme. Appropriate \gls{OOP} techniques can save a lot of time when adapting the same code for different schemes. Schematic defined in figure \ref{fig:finite-diff-prog-schema} describes the approach used in sequential programs, but it can be also used as base for parallel code.

\newpage
Figure \ref{fig:visualization:sequential} visualizes the straightforward program execution on a single processor. It is easy to notice, that program has to calculate whole grid before stepping into next time iteration. In a sequential code, we won't start calculating next time step, until the last grid point of previous time step is not know. Program model shown in \ref{fig:visualization:sequential} visualizes only a part of main solution process loop from figure \ref{fig:finite-diff-prog-schema}, initialization and result saving steps are omitted.

\begin{figure}
	\centering
	\begin{tikzpicture}
		\draw [->] [black, thick] (-.25, 0.25) -- (-.25, -6.5);
		\node[black,rotate=90] at (-0.5, -3.375) {\emph{Simulation time}};
		\filldraw[fill=SkyBlue, draw=black] (0,0) rectangle (13,-1);
		\draw[step=1cm,black,thin] (0,0) grid (13,-1);
		\node[black] at (0.5, -0.5) {$0$};
		\node[black] at (1.5, -0.5) {$1$};
		\node[black] at (6.5, -0.5) {$...$};
		\node[black] at (12.5, -0.5) {$N-1$};
		\node[black] at (-1.25, -0.5) {$t_{0}$};
		\node[black] at (6.5, -1.5) {\texttt{Processor 0.}};
		
		\filldraw[fill=SkyBlue, draw=black] (0,-2) rectangle (13,-3);
		\draw[step=1cm,black,thin] (0,-2) grid (13,-3);		
		\node[black] at (0.5, -2.5) {$0$};
		\node[black] at (1.5, -2.5) {$1$};
		\node[black] at (6.5, -2.5) {$...$};
		\node[black] at (12.5, -2.5) {$N-1$};
		\node[black] at (-1.25, -2.5) {$t + \Delta t$};
		\node[black] at (6.5, -3.5) {\texttt{Processor 0.}};
		
		\draw [->] [black] (6.5, -4) -- (6.5, -4.75);
		
		\filldraw[fill=SkyBlue, draw=black] (0,-5) rectangle (13,-6);
		\draw[step=1cm,black,thin] (0,-5) grid (13,-6);
		\node[black] at (0.5, -5.5) {$0$};
		\node[black] at (1.5, -5.5) {$1$};
		\node[black] at (6.5, -5.5) {$...$};
		\node[black] at (12.5, -5.5) {$N-1$};
		\node[black] at (-1.25, -5.5) {$\text{Final }t$};
		\node[black] at (6.5, -6.5) {\texttt{Processor 0.}};
	\end{tikzpicture}
	\caption{Calculating finite difference method on single processor.}
	\label{fig:visualization:sequential}
\end{figure}