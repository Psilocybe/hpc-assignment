\chapter{Results}
\label{ch:results}

All the achieved results come from the implementations of methods described in chapter \ref{ch:parallelization}. Code listings are available in the Appendix \ref{ch:listings}. Program was compiled and tested on the Cranfield University computer cluster, \Gls{astral} with the following commands:

\begin{lstlisting}[numbers=none,label=lst:compilation] 
	module load icc
	module load impi
	mpicxx -std=c++0x -lm *.cpp *.h
\end{lstlisting}

\section{Sequential and Parallel Results}
\label{sec:results:seqpar}

What can be easily seen on Figure \ref{fig:comparison:crank-nicsolson}, is that both solutions produced by two different programs overlap. Given the same simulation parameters, serial and parallel program give very similar results, although for long running simulations that cannot be true. Differences may become visible, least because of changed order of floating-point calculations, that will introduce various round-off error among solutions. That error will affect the least important decimal places, that cannot be seen in a table \ref{tab:norm-summary}.
\begin{figure}
	\centering
	\begin{tikzpicture}[scale=0.41]	 	
	\pgfplotsset{width=\textwidth}
	\begin{axis}[
		xlabel = {$x$},
		ylabel = {$f(x)$},
		xmin = 5, xmax = 13,
		minor y tick num = 1,
		y tick label style={/pgf/number format/.cd,fixed,precision=2},
		ymajorgrids=true,
		xmajorgrids=true,
		grid style=dashed,
		legend pos=north east]		
		\addcustomplotblank{charts/parallel-crank-0.9-1000.tsv}{red}{2}{Analytical}{6}
		\addcustomplotblank{charts/serial-crank-0.9-1000.tsv}{blue}{2}{Serial}{4}	 	\addcustomplot{charts/serial-crank-0.9-1000.tsv}{green}{1}{Parallel}{1}
	\end{axis}
	\end{tikzpicture}
	\caption{Comparison of numerical solutions for Crank-Nicolson scheme between serial and parallel algorithms. $CFL = 0.9$ and $1000$ grid points.}
	\label{fig:comparison:crank-nicsolson}
\end{figure}

Results shown in table \ref{tab:norm-summary} were obtained for the grid size equal to 1000 points. Parallel program was executed on 4 processors. As we can see, no matter the program was executed in serial or parallel, each application for every scheme provided us with same results at least up to six decimal places.

\begin{table}
	\caption{Comparison of error vector norms for parallel and sequential program.}
	\label{tab:norm-summary}
	\centering
	\ra{1.3}
	\begin{tabular}{@{}rrrrcrrr@{}}
		\toprule
		\multicolumn{8}{c}{Explicit Upwind} \\
		\midrule
		& \multicolumn{3}{c}{Serial} & \phantom{ph} & \multicolumn{3}{c}{Parallel} \\
		\cmidrule{2-4} \cmidrule{6-8}
		CFL & $L^{1}$ & $L^{2}$ & $L^{\infty}$ & & $L^{1}$ & $L^{2}$ & $L^{\infty}$ \\
		\midrule
		\emph{0.999} & 0.000008 & 0.001346 & 0.000439 & & 0.000008 & 0.001346 & 0.000439 \\
		\emph{0.900} & 0.000696 & 0.122114 & 0.038833 & & 0.000696 & 0.122114 & 0.038833 \\
		\midrule
		
		\multicolumn{8}{c}{Implicit Upwind} \\
		\midrule
		\emph{1.000} & 0.006175 & 0.914236 & 0.525930 & & 0.006175 & 0.914236 & 0.525930 \\
		\emph{1.100} & 0.006317 & 0.931006 & 0.268803 & & 0.006317 & 0.931006 & 0.268803 \\
		\midrule
		
		\multicolumn{8}{c}{Crank--Nicolson} \\ 
		\midrule
		\emph{1.000} & 0.000820 & 0.146800 & 0.043996 & & 0.000820 & 0.146800 & 0.043996 \\
		\emph{0.900} & 0.000770 & 0.138323 & 0.041326 & & 0.000770 & 0.138323 & 0.041326 \\
		\bottomrule
	\end{tabular}
\end{table}

\section{Communication Overhead Cost}
\label{sec:results:communication}

In parallel computing the cost of communication is the most crucial parameter, that we want to decrease. First and foremost, the size of the problem should be large enough to divide it across many processors. \Gls{computation-to-communication-ratio} has to be as small as possible, because nowadays processors are very efficient, while the connections between them are insufficient. Here, we calculate mentioned ratio based on devised stencils for each scheme and parallelization methods from chapter \ref{ch:parallelization}. We assume, that the most complex calculations are done while executing the scheme formula, i.e. calculating values of discretized grid points. Any side calculations like initialization or norm calculations are omitted.

\Gls{computation-to-communication-ratio} of the Explicit Upwind scheme can be calculated as number of elements send in each time step versus the total number of grid points to evaluate. In this case, processes have to exchange $p-1$ elements, where $p$ is equal to number of running processes. If the domain in discretized to $n$ grid points, the final ratio can be calculated as in \eqref{eq:ccr-explicit}.

\begin{equation}
	\label{eq:ccr-explicit}
	CCR_{explicit} = \frac{n}{p-1}
\end{equation}

Based on Implicit Upwind scheme \gls{stencil} \ref{fig:implicit-stencil}, we can easily find out, that the \gls{computation-to-communication-ratio} will be the same as previously. Processes have to exchange always only one value for each time iteration, thus ratio for Implicit Upwind is equal to \eqref{eq:ccr-implicit}.

\begin{equation}
	\label{eq:ccr-implicit}
	CCR_{implicit} = \frac{n}{p-1}
\end{equation}

Crank--Nicolson method has a complex \gls{stencil}, that requires exchange of both boundary values between each adjacent processes. Additionally, applying backward and forward substitutions cannot be done in parallel, so it causes extra idle time of processes. Calculating vector Q requires $2$ boundary values to be send and received by all working processes, while performing backward and forward substitution for $n$ points costs additional $2(p-1)$ exchanges. As previously, we are interested in general case for $n$ grid points, thus the overall ratio is equal to \eqref{eq:ccr-crank}.

\begin{equation}
\label{eq:ccr-crank}
	CCR_{Crank-Nicolson} = \frac{3n}{5p-3}
\end{equation}

\section{Performance}
\label{sec:results:performance}

The performance of parallel code is described by Amdahl's law \cite{wiki:amdahl}, that gives the theoretical \gls{speedup} in latency of the execution of a program solving the fixed sized problem, when it's resources are increased. In discussed case, we are solving problem that has a fixed size (domain discretized to 1000 and 10000 points), with increasing number of processors.

According to Amdahl's law, the theoretical \gls{speedup} of program is limited by a part, that cannot be parallelized. Looking at the figure \ref{fig:visualization:crank-thomas}, that shows communication pattern for a single time step of Crank--Nicolson method, we can predict that this scheme won't benefit from additional resources. As we can see in a figure \ref{fig:speedup:crank-nicolson}, \gls{speedup} for this scheme is low and increasing the number of processors shows negligible impact on execution time.

Explicit Upwind \ref{fig:speedup:explicit-upwind} shows gradual linear increase of \gls{speedup}, with additional number of processors for grid size equal 10000. Decreasing number of points by 10 times, causes the \gls{speedup} to drop significantly. This example shows the importance of the size of a problem and the connection with \gls{computation-to-communication-ratio}. 

	\begin{figure}[!htbp]
		\centering
		\begin{tikzpicture}[scale=0.7]	 	
		\pgfplotsset{width=\textwidth}
		\begin{axis}[
		xlabel = {\emph{number of processors}},
		ylabel = {\emph{speed--up}},
		xmin=1,xmax=33,
		ymin = 0,
		ymajorgrids=true,
		xmajorgrids=true,
		grid style=dashed,
		legend pos=north east
		]
		\addplot[domain=0:15,mark=*, cyan, line width = 1.5pt] {x};
		\addlegendentryexpanded{Theoretical};
		\addcustomplot{charts/explicit-speedup.tsv}{orange}{2}{1000 points}
		\addcustomplot{charts/explicit-speedup.tsv}{red}{4}{10000 points}
		\end{axis}
		\end{tikzpicture}
		\caption{Speed--up to processors for Explicit Upwind method.}
		\label{fig:speedup:explicit-upwind}
	\end{figure}

	Figure \ref{fig:speedup:implicit-upwind} presents the \gls{speedup} achieved by an Implicit Upwind scheme. This trend is very similar to the one of Explicit Upwind, because of the same communication pattern. 

	\begin{figure}[!htbp]
		\centering
		\begin{tikzpicture}[scale=0.7]	 	
		\pgfplotsset{width=\textwidth}
		\begin{axis}[
		xlabel = {\emph{number of processors}},
		ylabel = {\emph{speed--up}},
		xmin = 1, xmax=33,
		ymin = 0,
		ymajorgrids=true,
		xmajorgrids=true,
		grid style=dashed,
		legend pos=north east
		]
		\addplot[domain=0:12,mark=*, cyan, line width = 1.5pt] {x};
		\addlegendentryexpanded{Theoretical};
		\addcustomplot{charts/implicit-speedup.tsv}{orange}{2}{1000 points}
		\addcustomplot{charts/implicit-speedup.tsv}{red}{4}{10000 points}
		\end{axis}
		\end{tikzpicture}
		\caption{Speed--up to processors for Implicit Upwind method.}
		\label{fig:speedup:implicit-upwind}
	\end{figure}

	\begin{figure}[!htbp]
		\centering
		\begin{tikzpicture}[scale=0.7]	 	
			\pgfplotsset{width=\textwidth}
			\begin{axis}[
			xlabel = {\emph{number of processors}},
			ylabel = {\emph{speed--up}},
			xmin=1, xmax=33,
			ymin=0,
			ymajorgrids=true,
			xmajorgrids=true,
			grid style=dashed,
			legend pos=north east
			]
			\addplot[mark=*, cyan, line width = 1.5pt] {x};
			\addlegendentryexpanded{Theoretical};
			\addcustomplot{charts/crank-speedup.tsv}{orange}{2}{1000 points}
			\addcustomplot{charts/crank-speedup.tsv}{red}{4}{10000 points}
			\end{axis}
		\end{tikzpicture}
		\caption{Speed--up to processors for Crank--Nicolson method.}
		\label{fig:speedup:crank-nicolson}
	\end{figure}

	In each of the discussed charts, parallel schemes behaved better for larger discretization of the grid. That can be explained, by higher \gls{computation-to-communication-ratio} which is strongly connected with the grid size. When this ratio decreases, it means that more communication is performed and processors spend more time on sending and receiving data, than on doing the actual calculations. This effect occurs when the number of processors is to big for given problem, or when the size of the problem is just to small to be divided across many resources. In figures \ref{fig:speedup:explicit-upwind} and \ref{fig:speedup:implcit-upwind} for 1000 grid size, we can see, that when the local grid size for each of processors is around 100 points (10-12 processors), the \gls{speedup} starts to decrease.

\section{External Algebra Solver Impact}
\label{sec:results:external}

	To implement Crank--Nicolson method we tested \gls{eigen}, an external library for linear algebra. \Gls{eigen} provides tested and efficient data types and algorithms that can be used directly for implementing this method. Knowing the nature of Crank--Nicolson method, we mostly used \emph{LU} package, that contains methods performing LU factorization with partial pivoting and solving system of linear equations.
	
	Results presented in figure \ref{fig:eigen} shows, that solution produced with help of external library was unsatisfactory, when compared to our own implementation. Knowing the form of a matrix \emph{A}, Crank--Nicolson problem can be solved more efficiently, regarding the memory usage and number of performed operations. Implementing Thomas algorithm to solve the 3-diagonal matrix gives better results, because its lower complexity $\mathcal{O}(n^2)$. Additionally, we save the memory by storing only three single values of each diagonal, instead of keeping whole matrix within the operational memory. Tests performed on local PC have shown, that using \gls{eigen} in parallel code will not bring any positive benefits.
	
	Moreover, \Gls{eigen} library was not able to deal with grid size above 8000 points. Figure \ref{fig:eigen} shows, that knowledge about Crank--Nicolson method nature combined with own implementation on Thomas algorithm results in very efficient algorithm, that cannot be compared to general use linear algebra library performance.

	\begin{figure}[!htbp]
		\centering
		\begin{tikzpicture}[scale=0.7]	 	
			\pgfplotsset{width=\textwidth}
			\begin{axis}[
			xlabel = {\emph{grid size}},
			ylabel = {\emph{execution time, [s]}},
			x tick label style={/pgf/number format/.cd,%
				scaled x ticks = false,
				set thousands separator={},
				fixed},
			xmin=1000,xmax=10000, 
			ymin=0,
			ymajorgrids=true,
			xmajorgrids=true,
			grid style=dashed,
			legend pos=north west
			]
			\addcustomplot{charts/eigen-own.tsv}{blue}{1}{Own Implementation}
			\addplot[
				smooth,
				scatter, color=red,
				scatter/@pre marker code/.style={/tikz/mark size=1.5pt},
				scatter/@post marker code/.style={},
				line width = 1.2pt,
				skip coords between index={5}{6}
			]
			table[x index=0,y index=2, col sep=tab] {charts/eigen-own.tsv};
			\addlegendentryexpanded{Eigen Library}
			\end{axis}
		\end{tikzpicture}
		\caption{Performance comparison for Crank--Nicolson method.}
		\label{fig:eigen}
	\end{figure}