\section{Explicit Upwind}
\label{sep:parallelization:explicit}

Based on Explicit Upwind formula \ref{eq:explicitupwind} and its \gls{stencil} \ref{fig:explicit-stencil}, we can deduce that each processor, except the one that will be responsible for last chunk of grid, will have to send last local value from previous step to the next processor. In another words, to calculate the value of a first local grid point, node has to be provided with the left boundary value from a preceding node. This process is described in figure \ref{fig:visualization:explicit}. Before start of calculations for current time step, \texttt{P0} sends the last value from his local grid to the next processor \texttt{P1}, \texttt{P1} sends his value to \texttt{P2}, and so on. This pattern allows concurrent work of all processors through the whole simulation time and reduces their idle time to minimum.

There are no restrictions on the number of processors. Any specified amount of processors can be chosen, and it will be valid for this method.
\begin{figure}
	\centering
	\begin{tikzpicture}
		\draw [->] [black, thick] (-0.25, 2.25) -- (-0.25, -4.5);
		\node[black,rotate=90] at (-0.5, -1.125) {\emph{Simulation time}};
		
		%Initial step
		\node[black] at (-1.25,1.5) {$t_{0}$};
		\filldraw[fill=SkyBlue, draw=black] (0,2) rectangle (3,1);
		\draw[step=1cm,black,thin] (0,2) grid (3,1);
		\node[black] at (0.5,1.5) {$0$};
		\node[black] at (1.5,1.5) {$...$};
		\node[black] at (2.5,1.5) {$L-1$};
		\node[black] at (1.5,0.75) {\texttt{P0}};
		
		\filldraw[fill=SkyBlue, draw=black] (4,2) rectangle (7,1);
		\draw[step=1cm,black,thin] (4,2) grid (7,1);
		\node[black] at (4.5,1.5) {$L0$};
		\node[black] at (5.5,1.5) {$...$};
		\node[black] at (6.5,1.5) {$L-1$};
		\node[black] at (5.5,0.75) {\texttt{P1}};
		
		\filldraw[fill=SkyBlue, draw=black] (8,2) rectangle (11,1);
		\draw[step=1cm,black,thin] (8,2) grid (11,1);
		\node[black] at (8.5,1.5) {$L0$};
		\node[black] at (9.5,1.5) {$...$};
		\node[black] at (10.5,1.5) {$L-1$};
		\node[black] at (9.5,0.75) {\texttt{P3}};
		
		\filldraw[fill=SkyBlue, draw=black] (12,2) rectangle (15,1);
		\draw[step=1cm,black,thin] (12,2) grid (15,1);
		\node[black] at (12.5,1.5) {$L0$};
		\node[black] at (13.5,1.5) {$...$};
		\node[black] at (14.5,1.5) {$N-1$};
		\node[black] at (13.5,0.75) {\texttt{PN}};
		
		%First step
		\node[black] at (-1.25,-0.5) {$t_{0} + \Delta t$};
		\filldraw[fill=SkyBlue, draw=black] (0,0) rectangle (3,-1);
		\draw[step=1cm,black,thin] (0,0) grid (3,-1);
		\node[black] at (0.5, -0.5) {$0$};
		\node[black] at (1.5, -0.5) {$...$};
		\node[black] at (2.5, -0.5) {$L-1$};
		\node[black] at (1.5,-1.25) {\texttt{P0}};
		
		\node[black] at (3.5, 0.5) {\emph{LBD $t_{n}$}};
		\draw [->] [black] (2.5,0) -- (2.5,0.3) -- (4.5,0.3) -- (4.5,0);
		
		\filldraw[fill=SkyBlue, draw=black] (4,0) rectangle (7,-1);
		\draw[step=1cm,black,thin] (4,0) grid (7,-1);
		\node[black] at (4.5, -0.5) {$L0$};
		\node[black] at (5.5, -0.5) {$...$};
		\node[black] at (6.5, -0.5) {$L-1$};
		\node[black] at (5.5,-1.25) {\texttt{P1}};

		\node[black] at (7.5, 0.5) {\emph{LBD $t_{n}$}};
		\draw [->] [black] (6.5,0) -- (6.5,0.3) -- (8.5,0.3) -- (8.5,0);
		
		\filldraw[fill=SkyBlue, draw=black] (8,0) rectangle (11,-1);
		\draw[step=1cm,black,thin] (8,0) grid (11,-1);
		\node[black] at (8.5, -0.5) {$L0$};
		\node[black] at (9.5, -0.5) {$...$};
		\node[black] at (10.5, -0.5) {$L-1$};
		\node[black] at (9.5,-1.25) {\texttt{P3}};
		
		\node[black] at (11.5, 0.5) {\emph{LBD $t_{n}$}};
		\draw [->] [black,dashed] (10.5,0) -- (10.5,0.3) -- (12.5,0.3);
		
		\filldraw[fill=SkyBlue, draw=black] (12,0) rectangle (15,-1);
		\draw[step=1cm,black,thin] (12,0) grid (15,-1);
		\node[black] at (12.5, -0.5) {$L0$};
		\node[black] at (13.5, -0.5) {$...$};
		\node[black] at (14.5, -0.5) {$N-1$};
		\node[black] at (13.5,-1.25) {\texttt{PN}};
		
		\draw [->] [black] (7.5, -1) -- (7.5, -2);
		
		%Last step
		\node[black] at (-1.25,-3.5) {max $t$};
		\filldraw[fill=SkyBlue, draw=black] (0,-3) rectangle (3,-4);
		\draw[step=1cm,black,thin] (0,-3) grid (3,-4);
		\node[black] at (0.5, -3.5) {$0$};
		\node[black] at (1.5, -3.5) {$...$};
		\node[black] at (2.5, -3.5) {$L-1$};
		\node[black] at (1.5,-4.25) {\texttt{P0}};
		
		\node[black] at (3.5, -2.5) {\emph{LBD $t_{n}$}};
		\draw [->] [black] (2.5,-3) -- (2.5,-2.7) -- (4.5,-2.7) -- (4.5,-3);
		
		\filldraw[fill=SkyBlue, draw=black] (4,-3) rectangle (7,-4);
		\draw[step=1cm,black,thin] (4,-3) grid (7,-4);
		\node[black] at (4.5, -3.5) {$L0$};
		\node[black] at (5.5, -3.5) {$...$};
		\node[black] at (6.5, -3.5) {$L-1$};
		\node[black] at (5.5,-4.25) {\texttt{P1}};
		
		\node[black] at (7.5, -2.5) {\emph{LBD $t_{n}$}};
		\draw [->] [black] (6.5,-3) -- (6.5,-2.7) -- (8.5,-2.7) -- (8.5,-3);
		
		\filldraw[fill=SkyBlue, draw=black] (8,-3) rectangle (11,-4);
		\draw[step=1cm,black,thin] (8,-3) grid (11,-4);
		\node[black] at (8.5, -3.5) {$L0$};
		\node[black] at (9.5, -3.5) {$...$};
		\node[black] at (10.5, -3.5) {$L-1$};
		\node[black] at (9.5,-4.25) {\texttt{P3}};
		
		\node[black] at (11.5, -2.5) {\emph{LBD $t_{n}$}};
		\draw [->] [black,dashed] (10.5,-3) -- (10.5,-2.7) -- (12.5,-2.7);
		
		\filldraw[fill=SkyBlue, draw=black] (12,-3) rectangle (15,-4);
		\draw[step=1cm,black,thin] (12,-3) grid (15,-4);
		\node[black] at (12.5, -3.5) {$L0$};
		\node[black] at (13.5, -3.5) {$...$};
		\node[black] at (14.5, -3.5) {$N-1$};
		\node[black] at (13.5,-4.25) {\texttt{PN}};
	\end{tikzpicture}
	\caption{Calculating Explicit Upwind in parallel on N processors.}
	\label{fig:visualization:explicit}
\end{figure}

