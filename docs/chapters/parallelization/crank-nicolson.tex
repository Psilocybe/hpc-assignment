\section{Crank--Nicolson}
\label{sep:parallelization:crank}

By looking at the Crank--Nicolson method \gls{stencil} \ref{fig:stencil:crank-nicolson}, we can notice that this finite difference scheme is far more complex than previously discussed. To solve a system of linear equations in the form of \eqref{eq:crank-nicolson:matrix}, we need to devise a different algorithm than we used in previous cases. The biggest advantage of this scheme, is that the matrix \emph{A} is a 3-diagonal matrix with constant values on each of the diagonals. To solve that problem, we split it into 2 steps:
\begin{enumerate}
	\item Perform LU--decomposition on matrix \emph{A} and store the results in all cores.
	\item In each time step apply scheme and Backward and Forward substitutions using \emph{L} and \emph{U} matrices.
\end{enumerate}
Because the matrix \emph{A} is the same across all the time iterations, so will be the matrix \emph{L} and \emph{U}. By using this method, we lose time to calculate these matrices, but we gain a lot during the calculations of the grid values. The LU--decomposition time complexity for the problem of size $N\times N$ can be designated as $\mathcal{O}(n^{3})$, but we have to calculate it only once. Time cost of Backward and Forward substitutions executed in every time step is defined as $\mathcal{O}(n^{2})$, which compared to complexity $\mathcal{O}(n^{3})$ of solving linear system by matrix inversion is a huge progress.

\paragraph{Parallel LU Matrix Decomposition}
Exploiting recursive nature of LU--decomposition, leads to full--recursive--doubling algorithm. Parallel LU--decomposition version of this procedure was devised by Velde \cite{velde} and implemented in details by Karniadakis \cite{karniadakis}. This algorithm is developed to work with 3--diagonal matrices as in \eqref{eq:three-diagonal-matrix}. The decomposition of this matrix looks like in \eqref{eq:three-diagonal-matrix-lu}.

\begin{equation}
\label{eq:three-diagonal-matrix}
	\text{\huge A =} 
	\begin{bmatrix}
		\sigma_0     & \tau_1   &		     &              & \bigzeroup   &\\
		\rho_1       & \sigma_1 & \tau_2     &              &              &\\ 
		             & \ddots   & \ddots     & \ddots       &              &\\
		             &          & \rho_{N-2} & \sigma_{N-2} & \tau_{N-1}   &\\
		\bigzerodown &          & 	         & \rho_{N-1}   & \sigma_{N-1} &\\					
\end{bmatrix}
\end{equation}

\begin{equation}
\label{eq:three-diagonal-matrix-lu}
	\text{\huge L =}
	\begin{bmatrix}
		1     & 0   &		     &              & \bigzeroup   &\\
		\lambda_1       & 1 & 0     &              &              &\\ 
		             & \ddots   & \ddots     & \ddots       &              &\\
		             &          & \lambda_{N-2} & 1 & 0   &\\
		\bigzerodown &          & 	         & \lambda_{N-1}   & 1 &\\					
	\end{bmatrix}
	\text{\huge U =}
	\begin{bmatrix}
	\mu_0    & \upsilon_1   &		     &              & \bigzeroup   &\\
	0       & \mu_1 & 0     &              &              &\\ 
	& \ddots   & \ddots     & \ddots       &              &\\
	&          & 0 & \mu_{N-2} & \upsilon_{N-1}   &\\
	\bigzerodown &          & 	         & 0   & \mu_{N-1} &\\					
	\end{bmatrix}
\end{equation}

From this two matrices, we can deduce recursion relations for coefficients $\mu_i$, $\lambda_i$ and $\upsilon_i$ as follows:

\begin{align}
	\sigma_0 &= \mu_0 \label{eq:sigma0}\\
	\sigma_i &= \lambda_i\upsilon_i+\mu_i \label{eq:simgai}\\
	\tau_i &= \upsilon_i \label{eq:taui}\\
	\rho_i &= \lambda_i\upsilon_{i-1} \label{eq:rhoi} 
\end{align}

In \crefrange{eq:sigma0}{eq:rhoi} $i=0,2,..., N-1$. Solving coefficients $\tau_i$ is straightforward, but for $\sigma_i$ and $\rho_i$ we will relay on their recursive nature. Combining equations \eqref{eq:taui} and \eqref{eq:rhoi} into \eqref{eq:simgai} yields a rational recursion relationship for the unknown coefficients $\mu_i$:
\begin{align}
	\label{eq:recursion}
	\mu_i &= \sigma_i - \lambda_i\upsilon_i \nonumber \\
		  &= \sigma_i - \frac{\rho_i}{\mu_{i-1}} \tau_{i}\\
		  &= \frac{\sigma_i\mu_{i-1} - \rho_i\tau_{i}}{\mu_{i-1} + 0}	\nonumber
\end{align}

\begin{wraptable}{r}{0.35\textwidth}
	\centering
	\caption{LU--decomposition full--recursive--doubling algorithm communication pattern for 8 processors.}
	\label{tab:full-recursive-doubling-stages}
	\begin{tabular}{@{}ccc@{}}
		\toprule
		Stage 1. & Stage 2. & Stage 3. \\
		\midrule
		\texttt{$P_0$\rightarrow $P_1$} & \texttt{$P_0$\rightarrow $P_2$} & \texttt{$P_0$\rightarrow $P_4$}\\
		\texttt{$P_1$\rightarrow $P_2$} & \texttt{$P_1$\rightarrow $P_3$} & \texttt{$P_1$\rightarrow $P_4$}\\
		\texttt{$P_2$\rightarrow $P_3$} & \texttt{$P_2$\rightarrow $P_4$} & \texttt{$P_2$\rightarrow $P_6$}\\
		\texttt{$P_3$\rightarrow $P_4$} & \texttt{$P_3$\rightarrow $P_5$} & \texttt{$P_3$\rightarrow $P_7$}\\
		\texttt{$P_4$\rightarrow $P_5$} & \texttt{$P_4$\rightarrow $P_6$} &\\
		\texttt{$P_5$\rightarrow $P_6$} & \texttt{$P_5$\rightarrow $P_7$} &\\
		\texttt{$P_6$\rightarrow $P_7$} & &\\
		\bottomrule
	\end{tabular}
\end{wraptable}

Parallelization of this algorithm is done by using full--recursive--doubling procedure on the sequence of $2\times 2$ matrices as follows:

\begin{equation}
R_j =
	\begin{bmatrix}
		\sigma_i & -\tau_m\rho_i \\
		1        & 0 \\
	\end{bmatrix},
\end{equation}

\noindent
where $i=1,2,...,N-1$. Assuming $\tau_0=\rho_0=0$ and using M{\"o}bius transformations:

\begin{equation}
	T_i=R_iR_{i-1}...R_0
\end{equation}

\noindent
the final value of $\mu_i$ can be calculated as:

\begin{equation}
	\mu_i = \frac{\binom{1}{0}^{t}T_j\tbinom{1}{1}}{\binom{0}{1}^{t}T_j\tbinom{1}{1}}
\end{equation}

Taking into account the fact, that constructing the matrix \emph{A} for the Crank--Nicolson method is trivial, we can assume that every process will be able to reconstruct that matrix in constant time. The first task is to assign equal rows number of the \emph{A} matrix per each processor, then following steps are performed:

\begin{enumerate}
	\item Each process $P_i$ forms the matrices $R_k$, where subscript $k$ indicates the \emph{A} row indices for which current process is responsible.
	\item Calculate the matrix $S_i=R_{k\_max}R_{k\_max-1}...R_{k\_min}$.
	\item Using the full--recursive--doubling communication pattern (see table \ref{tab:full-recursive-doubling-stages}) distribute and merge the $S_j$ matrices (see table \ref{tab:full-recursive-doubling-distribution}).
	\item Each process $P_i$ calculates the local coefficients $\mu_k(k\_min\le k \le k\_max)$ based on local $R_k$ and received matrices.
	\item For processes $P_0$ to $P_{last-1}$ send local $\mu_{k\_max}$ to process with identifier higher by 1.
	\item On each process $P_i$ calculate the local coefficients $\lambda_k(k\_min\le k \le k\_max)$ based on local $\mu_k$ coefficients and values from previous step.
	\item Distribute $\mu_k$ and $\lambda_k$ to all processes, so every single process has full \emph{L} and \emph{U} matrices.
	\item Calculate the local forward and backward substitution.
\end{enumerate}

\begin{table}
	\centering
	\caption{Distribution and merging of processes local $S_j$ matrices for each communication stage - example for 8 processes.}
	\label{tab:full-recursive-doubling-distribution}
	\begin{tabular}{@{}ccccccccc@{}}
		\toprule
		& \texttt{$P_0$} & \texttt{$P_1$} & \texttt{$P_2$} & \texttt{$P_3$} & \texttt{$P_4$} & \texttt{$P_5$} & \texttt{$P_6$} & \texttt{$P_7$} \\
		\midrule
		Stage 0. & $S_0$ & $S_1$ & $S_2$ & $S_3$ & $S_4$ & $S_5$ & $S_6$ & $S_7$ \\
		Stage 1. &  & $S_1S_0$ & $S_2S_1$ & $S_3S_2$ & $S_4S_3$ & $S_5S_4$ & $S_6S_5$ & $S_7S_6$\\
		Stage 2. &  &  & $S_2S_1S_0$ & $S_3S_2S_1S_0$ & $S_4S_3S_2S_1$ & $S_5S_4S_3S_2$ & $S_6S_5S_4S_3$ & $S_7S_6S_5S_4$\\
		Stage 3. &  &  &  &  & $S_4S_3S_2S_1S_0$ & $S_5*...*S_0$ & $S_6*...*S_0$ & $S_7*...*S_0$\\
		\bottomrule
	\end{tabular}
\end{table}

The communication pattern presented in table \ref{tab:full-recursive-doubling-stages} introduces certain limitations, when it comes up to selecting the number of processes to launch. Because the number of communication stages is equal to $\log_2P$, where $P$ is equal to amount of processes, we have to select a number of processes used equal to positive integer power of 2.

\paragraph{Parallel Computation of Scheme}
Figure \ref{fig:visualization:crank-thomas} visualizes the second part of calculating the grid values. Step number one is related to determining the vector Q, that is to be used during forward and backward substitutions. Next two steps are backward and forward substitutions, in that order.

\begin{figure}
	\centering
	\begin{tikzpicture}
		\draw [->] [black, thick] (-0.25, 1) -- (-0.25, -18.5);
		
		%Vector q
		\node[black,rotate=90] at (-0.5, -0.5) {\emph{Vector Q}};
		\filldraw[fill=SkyBlue, draw=black] (0,0) rectangle (3,-1);
		\draw[step=1cm,black,thin] (0,0) grid (3,-1);
		\node[black] at (0.5, -0.5) {$0$};
		\node[black] at (1.5, -0.5) {$...$};
		\node[black] at (2.5, -0.5) {$L-1$};
		\node[black] at (1.5,-1.25) {\texttt{P0}};
		
		\node[black] at (3.5, 0.5) {\emph{LBD}};
		\draw [->] [black] (2.5,0) -- (2.5,0.3) -- (4.5,0.3) -- (4.5,0);
		
		\node[black] at (3.5, -1.5) {\emph{RBD}};
		\draw [->] [black] (4.5,-1) -- (4.5,-1.3) -- (2.5,-1.3) -- (2.5,-1);
		
		\filldraw[fill=SkyBlue, draw=black] (4,0) rectangle (7,-1);
		\draw[step=1cm,black,thin] (4,0) grid (7,-1);
		\node[black] at (4.5, -0.5) {$L0$};
		\node[black] at (5.5, -0.5) {$...$};
		\node[black] at (6.5, -0.5) {$L-1$};
		\node[black] at (5.5,-1.25) {\texttt{P1}};
		
		\node[black] at (7.5, 0.5) {\emph{LBD}};
		\draw [->] [black] (6.5,0) -- (6.5,0.3) -- (8.5,0.3) -- (8.5,0);
		
		\node[black] at (7.5, -1.5) {\emph{RBD}};
		\draw [->] [black] (8.5,-1) -- (8.5,-1.3) -- (6.5,-1.3) -- (6.5,-1);
		
		\filldraw[fill=SkyBlue, draw=black] (8,0) rectangle (11,-1);
		\draw[step=1cm,black,thin] (8,0) grid (11,-1);
		\node[black] at (8.5, -0.5) {$L0$};
		\node[black] at (9.5, -0.5) {$...$};
		\node[black] at (10.5, -0.5) {$L-1$};
		\node[black] at (9.5,-1.25) {\texttt{P3}};
		
		\node[black] at (11.5, 0.5) {\emph{LBD}};
		\draw [->] [black,dashed] (10.5,0) -- (10.5,0.3) -- (12.5,0.3);
		
		\node[black] at (11.5, -1.5) {\emph{RBD}};
		\draw [->] [black,dashed] (12.5,-1) -- (12.5,-1.3) -- (10.5,-1.3);
		
		\filldraw[fill=SkyBlue, draw=black] (12,0) rectangle (15,-1);
		\draw[step=1cm,black,thin] (12,0) grid (15,-1);
		\node[black] at (12.5, -0.5) {$L0$};
		\node[black] at (13.5, -0.5) {$...$};
		\node[black] at (14.5, -0.5) {$N-1$};
		\node[black] at (13.5,-1.25) {\texttt{PN}};
		
		\node[black] at (7.5, 1) {\emph{LBD}};
		\draw [->] [black] (14.5,0) -- (14.5,0.8) -- (0.5,0.8) -- (0.5,0);
		
		\node[black] at (7.5, -2) {\emph{RBD}};
		\draw [->] [black] (0.5,-1) -- (0.5,-1.8) -- (14.5,-1.8) -- (14.5,-1);
		
		\draw [-] [black,dotted] (-0.5,-2.25) -- (15.5,-2.25);
		
		%Forward substitution
		\node[black,rotate=90] at (-0.5, -6.5) {\emph{Backward Substitution}};
		%one
		\filldraw[fill=SkyBlue, draw=black] (0,-3) rectangle (3,-4);
		\draw[step=1cm,black,thin] (0,-3) grid (3,-4);
		\node[black] at (0.5, -3.5) {$0$};
		\node[black] at (1.5, -3.5) {$...$};
		\node[black] at (2.5, -3.5) {$L-1$};
		\node[black] at (1.5,-4.25) {\texttt{P0}};
		
		\node[black] at (3.5, -2.5) {$y_{L-1}^{n+1}$};
		\draw [->] [black] (2.5,-3) -- (2.5,-2.7) -- (4.5,-2.7) -- (4.5,-3);
		
		\filldraw[fill=OrangeRed, draw=black] (4,-3) rectangle (7,-4);
		\draw[step=1cm,black,thin] (4,-3) grid (7,-4);
		\node[black] at (4.5, -3.5) {$L0$};
		\node[black] at (5.5, -3.5) {$...$};
		\node[black] at (6.5, -3.5) {$L-1$};
		\node[black] at (5.5,-4.25) {\texttt{P1}};
		
		\filldraw[fill=OrangeRed, draw=black] (8,-3) rectangle (11,-4);
		\draw[step=1cm,black,thin] (8,-3) grid (11,-4);
		\node[black] at (8.5, -3.5) {$L0$};
		\node[black] at (9.5, -3.5) {$...$};
		\node[black] at (10.5, -3.5) {$L-1$};
		\node[black] at (9.5,-4.25) {\texttt{P3}};
		
		\filldraw[fill=OrangeRed, draw=black] (12,-3) rectangle (15,-4);
		\draw[step=1cm,black,thin] (12,-3) grid (15,-4);
		\node[black] at (12.5, -3.5) {$L0$};
		\node[black] at (13.5, -3.5) {$...$};
		\node[black] at (14.5, -3.5) {$N-1$};
		\node[black] at (13.5,-4.25) {\texttt{PN}};
		
		%two
		\filldraw[fill=OrangeRed, draw=black] (0,-5) rectangle (3,-6);
		\draw[step=1cm,black,thin] (0,-5) grid (3,-6);
		\node[black] at (0.5, -5.5) {$0$};
		\node[black] at (1.5, -5.5) {$...$};
		\node[black] at (2.5, -5.5) {$L-1$};
		\node[black] at (1.5,-6.25) {\texttt{P0}};
		
		\filldraw[fill=SkyBlue, draw=black] (4,-5) rectangle (7,-6);
		\draw[step=1cm,black,thin] (4,-5) grid (7,-6);
		\node[black] at (4.5, -5.5) {$L0$};
		\node[black] at (5.5, -5.5) {$...$};
		\node[black] at (6.5, -5.5) {$L-1$};
		\node[black] at (5.5,-6.25) {\texttt{P1}};
		
		\node[black] at (7.5, -4.5) {$y_{L-1}^{n+1}$};
		\draw [->] [black] (6.5,-5) -- (6.5,-4.7) -- (8.5,-4.7) -- (8.5,-5);
		
		\filldraw[fill=OrangeRed, draw=black] (8,-5) rectangle (11,-6);
		\draw[step=1cm,black,thin] (8,-5) grid (11,-6);
		\node[black] at (8.5, -5.5) {$L0$};
		\node[black] at (9.5, -5.5) {$...$};
		\node[black] at (10.5, -5.5) {$L-1$};
		\node[black] at (9.5,-6.25) {\texttt{P3}};
		
		\filldraw[fill=OrangeRed, draw=black] (12,-5) rectangle (15,-6);
		\draw[step=1cm,black,thin] (12,-5) grid (15,-6);
		\node[black] at (12.5, -5.5) {$L0$};
		\node[black] at (13.5, -5.5) {$...$};
		\node[black] at (14.5, -5.5) {$N-1$};
		\node[black] at (13.5,-6.25) {\texttt{PN}};
		
		%three
		\filldraw[fill=OrangeRed, draw=black] (0,-7) rectangle (3,-8);
		\draw[step=1cm,black,thin] (0,-7) grid (3,-8);
		\node[black] at (0.5, -7.5) {$0$};
		\node[black] at (1.5, -7.5) {$...$};
		\node[black] at (2.5, -7.5) {$L-1$};
		\node[black] at (1.5,-8.25) {\texttt{P0}};
		
		\filldraw[fill=OrangeRed, draw=black] (4,-7) rectangle (7,-8);
		\draw[step=1cm,black,thin] (4,-7) grid (7,-8);
		\node[black] at (4.5, -7.5) {$L0$};
		\node[black] at (5.5, -7.5) {$...$};
		\node[black] at (6.5, -7.5) {$L-1$};
		\node[black] at (5.5,-8.25) {\texttt{P1}};
		
		\filldraw[fill=SkyBlue, draw=black] (8,-7) rectangle (11,-8);
		\draw[step=1cm,black,thin] (8,-7) grid (11,-8);
		\node[black] at (8.5, -7.5) {$L0$};
		\node[black] at (9.5, -7.5) {$...$};
		\node[black] at (10.5, -7.5) {$L-1$};
		\node[black] at (9.5,-8.25) {\texttt{P3}};
		
		\node[black] at (11.5, -6.5) {$y_{L-1}^{n+1}$};
		\draw [->] [black,dashed] (10.5,-7) -- (10.5,-6.7) -- (12.5,-6.7);
		
		\filldraw[fill=OrangeRed, draw=black] (12,-7) rectangle (15,-8);
		\draw[step=1cm,black,thin] (12,-7) grid (15,-8);
		\node[black] at (12.5, -7.5) {$L0$};
		\node[black] at (13.5, -7.5) {$...$};
		\node[black] at (14.5, -7.5) {$N-1$};
		\node[black] at (13.5,-8.25) {\texttt{PN}};
		
		%four
		\filldraw[fill=OrangeRed, draw=black] (0,-9) rectangle (3,-10);
		\draw[step=1cm,black,thin] (0,-9) grid (3,-10);
		\node[black] at (0.5, -9.5) {$0$};
		\node[black] at (1.5, -9.5) {$...$};
		\node[black] at (2.5, -9.5) {$L-1$};
		\node[black] at (1.5,-10.25) {\texttt{P0}};
		
		\filldraw[fill=OrangeRed, draw=black] (4,-9) rectangle (7,-10);
		\draw[step=1cm,black,thin] (4,-9) grid (7,-10);
		\node[black] at (4.5, -9.5) {$L0$};
		\node[black] at (5.5, -9.5) {$...$};
		\node[black] at (6.5, -9.5) {$L-1$};
		\node[black] at (5.5,-10.25) {\texttt{P1}};
		
		\filldraw[fill=OrangeRed, draw=black] (8,-9) rectangle (11,-10);
		\draw[step=1cm,black,thin] (8,-9) grid (11,-10);
		\node[black] at (8.5, -9.5) {$L0$};
		\node[black] at (9.5, -9.5) {$...$};
		\node[black] at (10.5, -9.5) {$L-1$};
		\node[black] at (9.5,-10.25) {\texttt{P3}};
		
		\filldraw[fill=SkyBlue, draw=black] (12,-9) rectangle (15,-10);
		\draw[step=1cm,black,thin] (12,-9) grid (15,-10);
		\node[black] at (12.5, -9.5) {$L0$};
		\node[black] at (13.5, -9.5) {$...$};
		\node[black] at (14.5, -9.5) {$N-1$};
		\node[black] at (13.5,-10.25) {\texttt{PN}};
		
		%Backward substitution
		\node[black,rotate=90] at (-0.5, -14.5) {\emph{Forward Substitution}};
		%one
		\filldraw[fill=OrangeRed, draw=black] (0,-11) rectangle (3,-12);
		\draw[step=1cm,black,thin] (0,-11) grid (3,-12);
		\node[black] at (0.5, -11.5) {$0$};
		\node[black] at (1.5, -11.5) {$...$};
		\node[black] at (2.5, -11.5) {$L-1$};
		\node[black] at (1.5,-12.25) {\texttt{P0}};
		
		\filldraw[fill=OrangeRed, draw=black] (4,-11) rectangle (7,-12);
		\draw[step=1cm,black,thin] (4,-11) grid (7,-12);
		\node[black] at (4.5, -11.5) {$L0$};
		\node[black] at (5.5, -11.5) {$...$};
		\node[black] at (6.5, -11.5) {$L-1$};
		\node[black] at (5.5,-12.25) {\texttt{P1}};
		
		\filldraw[fill=OrangeRed, draw=black] (8,-11) rectangle (11,-12);
		\draw[step=1cm,black,thin] (8,-11) grid (11,-12);
		\node[black] at (8.5, -11.5) {$L0$};
		\node[black] at (9.5, -11.5) {$...$};
		\node[black] at (10.5, -11.5) {$L-1$};
		\node[black] at (9.5,-12.25) {\texttt{P3}};
		
		\node[black] at (11.5, -10.5) {$x_{0}^{n+1}$};
		\draw [->] [black,dashed] (12.5,-11) -- (12.5,-10.7) -- (10.5,-10.7);
		
		\filldraw[fill=SkyBlue, draw=black] (12,-11) rectangle (15,-12);
		\draw[step=1cm,black,thin] (12,-11) grid (15,-12);
		\node[black] at (12.5, -11.5) {$L0$};
		\node[black] at (13.5, -11.5) {$...$};
		\node[black] at (14.5, -11.5) {$N-1$};
		\node[black] at (13.5,-12.25) {\texttt{PN}};
		
		%two
		\filldraw[fill=OrangeRed, draw=black] (0,-13) rectangle (3,-14);
		\draw[step=1cm,black,thin] (0,-13) grid (3,-14);
		\node[black] at (0.5, -13.5) {$0$};
		\node[black] at (1.5, -13.5) {$...$};
		\node[black] at (2.5, -13.5) {$L-1$};
		\node[black] at (1.5,-14.25) {\texttt{P0}};
		
		\filldraw[fill=OrangeRed, draw=black] (4,-13) rectangle (7,-14);
		\draw[step=1cm,black,thin] (4,-13) grid (7,-14);
		\node[black] at (4.5, -13.5) {$L0$};
		\node[black] at (5.5, -13.5) {$...$};
		\node[black] at (6.5, -13.5) {$L-1$};
		\node[black] at (5.5,-14.25) {\texttt{P1}};
		
		\node[black] at (7.5, -12.5) {$x_{0}^{n+1}$};
		\draw [->] [black] (8.5,-13) -- (8.5,-12.7) -- (6.5,-12.7) -- (6.5,-13);
		
		\filldraw[fill=SkyBlue, draw=black] (8,-13) rectangle (11,-14);
		\draw[step=1cm,black,thin] (8,-13) grid (11,-14);
		\node[black] at (8.5, -13.5) {$L0$};
		\node[black] at (9.5, -13.5) {$...$};
		\node[black] at (10.5, -13.5) {$L-1$};
		\node[black] at (9.5,-14.25) {\texttt{P3}};
		
		\filldraw[fill=OrangeRed, draw=black] (12,-13) rectangle (15,-14);
		\draw[step=1cm,black,thin] (12,-13) grid (15,-14);
		\node[black] at (12.5, -13.5) {$L0$};
		\node[black] at (13.5, -13.5) {$...$};
		\node[black] at (14.5, -13.5) {$N-1$};
		\node[black] at (13.5,-14.25) {\texttt{PN}};
		
		%three
		\filldraw[fill=OrangeRed, draw=black] (0,-15) rectangle (3,-16);
		\draw[step=1cm,black,thin] (0,-15) grid (3,-16);
		\node[black] at (0.5, -15.5) {$0$};
		\node[black] at (1.5, -15.5) {$...$};
		\node[black] at (2.5, -15.5) {$L-1$};
		\node[black] at (1.5,-16.25) {\texttt{P0}};
		
		\node[black] at (3.5, -14.5) {$x_{0}^{n+1}$};
		\draw [->] [black] (4.5,-15) -- (4.5,-14.7) -- (2.5,-14.7) -- (2.5,-15);
		
		\filldraw[fill=SkyBlue, draw=black] (4,-15) rectangle (7,-16);
		\draw[step=1cm,black,thin] (4,-15) grid (7,-16);
		\node[black] at (4.5, -15.5) {$L0$};
		\node[black] at (5.5, -15.5) {$...$};
		\node[black] at (6.5, -15.5) {$L-1$};
		\node[black] at (5.5,-16.25) {\texttt{P1}};
		
		\filldraw[fill=OrangeRed, draw=black] (8,-15) rectangle (11,-16);
		\draw[step=1cm,black,thin] (8,-15) grid (11,-16);
		\node[black] at (8.5, -15.5) {$L0$};
		\node[black] at (9.5, -15.5) {$...$};
		\node[black] at (10.5, -15.5) {$L-1$};
		\node[black] at (9.5,-16.25) {\texttt{P3}};
		
		\filldraw[fill=OrangeRed, draw=black] (12,-15) rectangle (15,-16);
		\draw[step=1cm,black,thin] (12,-15) grid (15,-16);
		\node[black] at (12.5, -15.5) {$L0$};
		\node[black] at (13.5, -15.5) {$...$};
		\node[black] at (14.5, -15.5) {$N-1$};
		\node[black] at (13.5,-16.25) {\texttt{PN}};
		
		%four
		\filldraw[fill=SkyBlue, draw=black] (0,-17) rectangle (3,-18);
		\draw[step=1cm,black,thin] (0,-17) grid (3,-18);
		\node[black] at (0.5, -17.5) {$0$};
		\node[black] at (1.5, -17.5) {$...$};
		\node[black] at (2.5, -17.5) {$L-1$};
		\node[black] at (1.5,-18.25) {\texttt{P0}};
		
		\filldraw[fill=OrangeRed, draw=black] (4,-17) rectangle (7,-18);
		\draw[step=1cm,black,thin] (4,-17) grid (7,-18);
		\node[black] at (4.5, -17.5) {$L0$};
		\node[black] at (5.5, -17.5) {$...$};
		\node[black] at (6.5, -17.5) {$L-1$};
		\node[black] at (5.5,-18.25) {\texttt{P1}};
		
		\filldraw[fill=OrangeRed, draw=black] (8,-17) rectangle (11,-18);
		\draw[step=1cm,black,thin] (8,-17) grid (11,-18);
		\node[black] at (8.5, -17.5) {$L0$};
		\node[black] at (9.5, -17.5) {$...$};
		\node[black] at (10.5, -17.5) {$L-1$};
		\node[black] at (9.5,-18.25) {\texttt{P3}};
		
		\filldraw[fill=OrangeRed, draw=black] (12,-17) rectangle (15,-18);
		\draw[step=1cm,black,thin] (12,-17) grid (15,-18);
		\node[black] at (12.5, -17.5) {$L0$};
		\node[black] at (13.5, -17.5) {$...$};
		\node[black] at (14.5, -17.5) {$N-1$};
		\node[black] at (13.5,-18.25) {\texttt{PN}};
	\end{tikzpicture}
	\caption{Crank--Nicolson parallel communication pattern.}
	\label{fig:visualization:crank-thomas}
\end{figure}
